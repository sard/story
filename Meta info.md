## Forward to myself:
This might be my first complete novel. I'm doing a discovery writing exercise. Write the full thing as no more than a second draft. Then set it down and write something new. The grain of this story can from my last day of my drive to Oregon. I did one full pass and discovered all the basic plot. Then started growing detail with a second pass. The plot is still malleable because I don't really know the answer that drives me to keep writing about this: is the narrative a delusion? Is she real?

That limited perspective of the narrative maintains both realities as possible. I doubt the narrative will ever give a conclusive answer. Which I find really frustrating but also might be the point. But once I know the answer for myself as a reader of what I wrote then the story will be concluded. There are so many more questions to ask and answers to find from this narrative. Hopefully this will distract me from the primary question driving me writing.

We'll see if this is the story that I complete. I hope it is. I truly think it's a great fit for this marathon and could help me grow into someone who knows they can write a novel. This is just my latest attempt toward becoming a writer.


## Cheesy back cover Summary
Gia has secrets. Some of which she knows are true. Others she's not so sure. It can be hard to balance trying to defend nature and start a family. Lose the former and all is lost. Lose the latter and what's the point. This can be further complicated by experiencing life with schizotypal. And that recently a fungus from the future has informed her that climate change is much more dire than human models predict.

## About the author (of the first draft)
First off let me say that I am a poser. Whatever accuracy towards the experiences I've written about comes from in person research. Just talking to people really. Secondly I think it's good to be a poser. It gives herd immunity to those doing the work. Additionally if I'm ever wrongly accused due to posing too well then I've potentially protected someone(s) doing that essential work. Which I can live with. I'm a writer. I can do my work from any old place.
### version that was on nanowrimo
First off let me say that I am a poser. Whatever accuracy towards the experiences I've written about comes from in person research. Just talking to people really. Secondly I think it's good to be a poser. It gives herd immunity to those doing the work. Additionally if I'm ever wrongly accused due to posing too well then I've potentially protected someone(s) doing that essential work. Which I can live with. I'm a writer, I guess. I can do my work from any old place.