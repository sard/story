# Part 1: Winter Solstice
## I
A few hundred miles from the woods Gia grew up in, memories of a childhood friend entered her mind. She was her first female friend. And she helped Gia realize that she was also a girl. They explored what gender meant together, in the woods, somehow. Gia couldn't remember here name though. It had been fifteen years since they last talked and eleven years Gia had visited her parents at their childhood home. This friend was her best friend from her earliest memory. And she was imaginary.

This was why Gia suggest that she and her partners visit her parents for thanksgiving week. By Christmas Gia's parents will move into there new home that is walking distance from Gia and her partner's house. They're all starting a family together. Gia was so excited. This week was the last week she could wander around her childhood woods. Gia's last chance to fulfill a promise she made fifteen years prior.

"What are you thinking about?" Olivia asked. She spoke softly as to not wake up Valorie and Ava sleeping in the backseats.

"My childhood. Excited to visit where I grew up."

"Yeah I'm excited to get to spend the week with your parents" Olivia said completely sincerely. Olivia had this way of saying things that said from anyone else you'd assume the opposite meaning.

"I think I'm at the beginning of a psychotic episode." Gia stated. She and Olivia have spent many hours discussing Gia's delusions. Olivia was someone she could be straightforward with in a way that was useful.

"Oh neat. How do you notice that is happening?" Playful curiosity, her default respond to most things. Depending on Gia's mood it felt dismissive or encouraging.

"As a kid I had an imaginary friend I'd play in the woods with."

"Was this the one that help you realize you were trans?"

"Yeah. We'd try out feminine outfit, play games, stuff like that. I didn't actually see her but she still felt real. Right before I came out my freshmen year of high school, I saw her and we talked. I think she was my first experience with psychosis. First one I can remember."

Olivia thought about her response for a moment. "What did y'all talk about?"

"It was so long ago. I don't really remember the exact conversation. But basically she was a fungus from the future here to prevent climate change."

"That's pretty cool. So did she look like a mushroom person or something?"

"No she looked like a regular girl my age at the time who also happened to be really cute. She explained that she could interface with my mind because I was schizophrenic and she had been practicing how throughout my childhood."

"So you where 14, 15 that was before you where diagnosed right?"

"Yeah I was diagnosed in college for something completely unrelated. Honestly I blocked out this disillusion and only became aware of it recently. I've actually never talked about it with anyone."

"I appreciate you sharing with me." Olivia said as she so often does.

"Yeah. And she told me that she was about my age and was raised to be compatible with communication with humans. That her mother was her main interface with what is essentially a hivemind of fungus that exist throughout the planet. That me and her were an attempt to bridge the communication gap between humans and her species."

"So like ambassadors?"

"Yeah basically. We talked for a really long time that day but I don't remember most of it. Just that it was night when I came back from the woods. Before I left she asked that I come back for one more conversation and that she'd prove that she was real."

"Did you?"

"Not yet."

"So that's why you never wanted to visit your parents before they moved."

"Yeah."

"And why we never visited them into now."

"I think so."

"How can I help."

"Yeah I guess I want the week to go well. Just help me stay grounded. I need someone to talk to about it that I can trust to not believe that the delusion is real."

"I can do that." She smiled. "Although it would be nice if such a thing did exist. Would be reassuring to know that it wasn't all on us humans."

They kept talking for the rest of the drive there. The conversation wasn't particularly memorable. But an the time engaging and a happy distraction. The sun was setting but the time they arrived at Gia's parents double wide. As Gia helped unpack and felt more and more disconnected from her body. She remembered her mom saying that she "looked fried." Olivia introduced folks and the interaction felt positive. She said something about how Gia drove the whole trip there. Gia carried her mostly empty bag into her childhood bedroom. There were two mattresses. One on the floor. Gia laid down in her old bed and tried to connect to her body.

---

"So this is the spot" Olivia said looking at a rock off to one side of the meadow.

"Yeah this is were I'd sit and play with my imaginary friend. And where we had our final conversation."

"Well second to final right?"

"Yeah I guess."

"Are you sure you want to do this right now? You seemed maybe hesitate when we on the walk over here."

Gia was surprised by Olivia insight. Gia couldn't remember what she said to indicate that to Olivia. And Olivia typically doesn't read non-verbals well. It looked like dawn or maybe dusk. The temperature was nice out. Gia didn't have her phone on her.

"What time is it?" Gia asked.

"6:30-ish" Olivia respond unhelpfully. Gia hesitated to ask if it was 6:30 AM or PM. Gia was didn't want to alert her that she apparently lost time. The point was to have this conversation. She didn't want Olivia's questions to pull her out of this reality.

"Yeah I want to do this. This is the right time for it. Just tell me what you notice after we leave the meadow."

"Okay for sure" Olivia smiled. She likes clear instructions on how to be helpful.

Gia sat down on the large limestone rock that was mostly flush with the ground. It was smooth to the touch. See looked over to Olivia who was looking away, past the meadow into the distance of the scraggly wood.

"Olivia?" Gia asked in an alarmed tone of voice "Wh-What are you looking at" her voice shook out. Olivia started to turn toward her in an unnatural movement. This wasn't Olivia. As her face peaked into Gia's perspective. A recognition that she was something demonic entered her awareness.

"Nothing really. Just trying to give you space to do you thing." She smiled looking toward Gia but not at her as she often does. Olivia was back.

"Can you sit beside me?"

"Sure!" A smiley Olivia held my hand and rested her head on my shoulder.

Gia's right hand connected to the reality Olivia lived in. She looked at her left hand. A memory flashed into her mind.

"I connect to you from a neural link they created in your left pinky. When the skin of the last segment come in contact with my mycelium we can talk." The memory of a teenage girl told Gia.

Gia looked down on her left side. There a strange mushroom grew. Gia had never seen it but she knew that was her friend. Gia placed her left hand next to the mushroom. Reality shifted.

The sky changed. The sun no longer below the tree line in one spot. It appeared as those the sun was shining from all directions around the treeline and above stars danced in a permanent twilight sky. And Olivia was gone.

Gia was alone. But she sensed that she called out her friends name maybe they would appear. But she could remember it.

"That's because you never gave me a name." A gentle voice of a woman on her left said. She was sitting down looking into the meadow. "I don't really have a name that translates into anything that would make sense."

"You got older" Gia noticed.

"So did you. It's been a long time since we last talked."

"I guess I didn't expect you to change for some reason."

"I have. Quite a lot actually" was that bitterness in her voice?

"I'm sorry I waited so long to come back."

"No I'm sorry" she sighed "Your here now. And that's enough."

"Last we talked you mention you would prove that you were real on our next conversation."

The face of perhaps the most uniquely beautiful woman Gia had ever seen deformed. It was like she already knew that I'm only doing this so I would feel okay abandoning her forever.

"I can't do that anymore. Preparations couldn't endure fifteen years of waiting. In theory with some time for preparations I could." she paused "Honestly I'm not sure I could prove it to you anymore. I sense this will be our last conversation. And that is okay. After we talk, my ego and my feelings for you will dissolve upon entering the mycelial collective."

"What was the plan if I had come back sooner?"

"Forest fire. The network prepared a section of forest through cultivating and culling tree, brush, etc via redistributing resources between plant. They can chemically start fires somehow and it would be a completely controlled burn. I was to tell you the when and where and the exact number of hectares that would burn. And after it happened you could research and confirm that the number match. The woods we cultivate are ever changing. They stopped struggling to maintain a predictable controlled burn after you left home and the collective gave up on the idea you'd ever return."

"All that for me?"

"Apparently we were the most promising pair that collective has tried to establish communication with. Our connection is special." She paused for a moment as fear filled her face. "Also I've been deceiving you."

"How so?"

"When you came back I was so excited. I've missed in so much. In your absence I've expanded my mycelium to be throughout your parents house. I've read their books they never remove from there shelf. Listen to there conversations. And even connected to" she paused "all kinds of things."

"I imagine you felt lonely. I don't feel deceived by that" Gia reassured.

"Well see you're actually laying down in your bed. My mycelium is also all over your room. Once your pinky touch me I may have perhaps over eagerly connected to you."

"But that" Gia paused. *That's why I can't remember how I got here.* "You pretended to be Olivia."

"Yes."

"How?"

"I can read your surface thoughts. I brought you here and tried to mimic what you expected from her."

"I want to leave. How do I leave."

"Will you come back?"

"Do I have a choice?"

"Of course you do. It's always been up to you. You have all the control here. All I can do is be patient and hope."

"I can't leave and choose when I come here."

"Of course you can. I'm sorry I tricked you."

---

Gia opened her eyes to see light from outside reflecting off her laminated mushroom foraging poster. She scanned it with her eyes passively. The sun had set and the porch behind her window was full of comforting voices.

*I guess that could have gone better.* Gia looked at her left hand then towards the window and got up. An uncertain body stepped out of the room. Gia made it to the front door which connected to the porch and waited. She heard Olivia saying something about whales and step through the entryway.

Valorie, Ava, Olivia, Mom, Dad sat on a wooden picnic table. The old deck wood felt good on her bare feet. The metal roof her uncle built blocked out the stars. Moths circled bulbs in various fixtures scattered around. One of Valorie's playlist was pleasantly audible from her Bluetooth speaker. Things were going well. *I just fell asleep after a long drive.* Gia thought then explained to the group once acknowledged. Gia sat down and took in the vibe. Everyone's words washed over her.

Did the words not matter because nothing was really being said by anyone? *Am I still just thinking of her.* Gia considered whether that counted as the second conversation. Was that really the resolution. Was it over? *Do I want that to be where I leave things?*

*No.*

Gia's Mom cut through "I made dinner. It's on the stove if you want any." There was a moment of silence. Olivia looked like she was about to say something.

"Yeah maybe" Gia said quickly. She wasn't hungry she just needed time to think. 

"I'll heat it up for you" her Mom continued and Gia followed the cues. After I few steps, nods, smiles, and thank yous, warm food was in front of her and she was sitting at the dinner table. Olivia was there too. Gia's Mom rejoined the others card game.

"You alright?" Olivia's voice felt so real.

"Could I get some water?" Gia asked. *I could have gotten that. I know where it is. I'm not a child.*

"Sure thing!" Olivia smiled "Where are you cups?" she continued to smile in her incomprehensible way. Gia pointed and within a few moments water was in her hands. She drank.

"I'm not really hungry. Do you want this?"

"Actually yeah" Olivia ate and they sat in a comfortable silence. Olivia slide the plate back over to her "Are you sure you don't want any?"

"Yeah. I don't think I'll be eating much for a bit." Gia explained. Olivia left the plate in front of Gia.

"So let's talk. What's going on?"

"How are things going with my parents?"

"Everyone seems to be getting along I think. I was surprised to see how sound asleep you were. I held you for a while. You must have been really tired."

"I don't think I was actually asleep. Was there anything else that seemed strange compared to how I usually sleep."

"Your body was really rigid I guess." An awkward silence started.

Olivia looked causal. She seemed perfectly comfortable in that silence. It didn't matter that Gia's responses were confusing. That's Olivia secret she's always confused. She looked comfortable. *I could say anything and it wouldn't be a big deal.* "I talked with her." Gia said softly.

"How did that go?" Olivia asked.

*Did she even know who her referred?* "She's older now"

"That makes sense. How was she?"

"Bad I think. Lonely. I abandoned her I guess."

"Right. Is that what she said or what you assume?" Olivia was taking this way to literally.

"It's all in my head so there's not really a meaningful distinction."

"That's fair sorry. So she's upset because you abandoned her?"

"No."

Olivia waited. She didn't know what to say. "Do you want to talk with her again?"

*No! This was it. I'm done. I want this to end. I want to be in reality.* Gia's body went cold and posture sunk. *Is this real right now?* Gia glanced at Olivia and reality shuttered. What was behind that concerned look.

"Maybe."

## II
Gia laid on a beach. The sun was out and it felt pleasantly warm. She was alone and the sounds of waves made her feel at peace. Gia had the vague notation that she was on an island. A bird that Gia would guess was a seagull lands in front of her. 

"Hello there" the bird squawks.

"A pleasure. What brings you to this island that we're on?" Gia asked.

"Fish" the bird stated. "And to talk with you."

"That's nice what shall we talk about? Perhaps I could hop on your back and we could fly around?" Gia suggested.

"I'm too small for that" the bird said quite rationally.

"Well why don't you eat this" Gia presented a flower that was in her hand "It's a magic flower" Gia lied "If you eat it you'll grow big and I could ride you."

"I don't believe in magic" the talking bird said.

"Right well what do you wish to talk about?"

"I was wondering if you wanted help sleeping?" the bird asked warmly.

"Aren't I already?"

"Maybe but say you were having together sleeping and I, a talking bird, could help you fall asleep and feel rested. Would you want me to do that?"

Gia considered her response. "Is there any potential harm or side effects?"

"No it should be fairly safe. Just precisely timed melatonin micro doses."

"But if you messed up the dose couldn't that like bork my sleep cycle or something."

"Well sure but I know what I'm doing. I went to bird school. It's perfectly safe."

"Well consider me informed and consenting doc. Wait but how do you know how to do that. I'm pretty sure this treatment is only theoretical."

"For humans sure. But I'm a bird."

"Right. Birds are cool. Love how you guys flying around all the time. Except for chickens. Poor chickens."

"I do dream of flying too you know. Being able to walk would be pretty neat also." the no longer bird said.

"So what are you then. If not a bird. Was bird school a lie!?" Gia felt herself drifting into warm unreality.

"I'm your friend" a soft voice said.

---

Gia awoke with Olivia holding her. *What a strange series of dreams.* Gia thought. *No.* She had actually gotten up in the middle there. She remembered brushing her teeth and her mouth felt as those that was the case.

"Did you get any sleep." Olivia asked in her ear softly. "I felt you moving around more than usual." Olivia always seemed aware of how Gia slept when they slept next to each other but also somehow seemed to get enough sleep.

"Yeah I feel great actually. What time is it?" Gia asked.

"I'd guess 7 or 8-ish maybe. Based on the light. it stopped being blue a bit ago." Both had their phones within arms reach but neither reached for them. Instead Gia turned toward Olivia and looked at her gorgeous face. She was looking slightly down and smiling. She kissed Gia's cheek once then a few more times.

Gia lingered in the warm with Olivia then entered the cool morning air of her bedroom. She left that room with Olivia silently watching. In a pile on the couch in the living adjoined to her bedroom was a bunch of her old clothes. Gia's mom asked her to look through them with they were here. On top was her old jacket she had worn most days in high school.

She'd patched it, hemmed it in places, and realized she missed it. *Why did I leave it? it's not like I stopped being a punk after I left.* Gia put it on. *Still fits. No surprise there.* Gia hadn't eaten since yesterday morning. She hurried past the kitchen. She heard her mom somewhere in the backroom but she didn't want to be noticed right now. She sat down in the main living room that adjoined to the front porch via the front door and laced up her hiking boots.

Gia managed to get into the woods without a single interaction. This was a great relief. She gently dropped the ability to mask she'd been holding onto. The mask of someone not considering engaging in an ongoing dialogue with her mycelium based friend. Gia was determined to test the reality of this by taking it seriously.

She got to the meadow quicker than she'd imagined. These woods were smaller in reality than in her mind. Paths she made exploring as a kid remained and she recognized very little as changing. This was the setting to the first half of her life. The second was vans, punk houses, occupations, and sky hammocks. For so long since leaving these woods she was part of a collective. Here she was herself.

Gia found the slab of limestone and brushed off leaves that had completely covered it. She sat down and feel the soft hardness of the smooth slab. Being careful not the harm the patches of fluffy green moss sharing the slab with her. *Very kind of them.*

No mushrooms where visible around the slab but it was no matter. Gia uncurled her left hand. She realized she'd been keeping in a fist. She gently let her pinky touch the soil. Most of her hand still on the limestone. The exposure of her pinky to soil seeming almost incidental. She looked off into the distance. And tried to imagine what Olivia was looking at in her dream. In that distance saw a green bird. Like one from a pet store. Gia looked closer and there was a few of them. She'd seen such a site in a city park once. Likely feral friends that escaped there cages and now struggled to survive.

It was weird to see them out here though. She focused on them for a while and listened and focused on their squawks. The sun was gone from the sky above and had decided to fill every part of the horizon leaving Gia in a permanent twilight. She was now in circled by strange curly mushrooms that were plump like a succulent but maybe a bit less.

The stars once still, broke their social tension and began mingling with each other. That's nice, I hope they make friends. The woods around her felt teemed with social interaction animals, grass, flowers, and trees all talking as a warm crowd. She was in an ideal city square environment. No cars trended here. The meadow was comforting and she was taking it in while she waited for her friend to arrive.

"I so glad you decided to meet with me again." the woman she was waiting for stated and sat down at her table in the set across from her. Gia hadn't notice until now that there was a beautiful, old brick building peaking its way through the treeline on the other end of the meadow. Behind Gia the comfort of a lively cafe.

"Of course. When we last spoke things didn't feel quite resolve. Also this is the first of our conversations I've chosen. So the others don't count. I promised I'd comeback once more to chat. So this is me doing that." Gia said trailing off.

"What do you think of the locale? I tried to pick an environment we'd both find pleasant."

"It's quite lovely. I'd love to live here" Gia joked.

"That is an option" the woman said coyly. Her smile was warm and comforting. She looked Gia in the eyes for a moment. Gia looked down to see their hand touching slightly on the table. She didn't move and looked back up.

"Why do you look like a human?" Gia asked as her hand moved slightly away.

"Well my body is subterranean so there isn't really a meaningful way visualize my physical appearance. Honestly I'm not even sure I could if I tried. Our brains are in many ways similar to humans, based on it in fact. Visual representation is common way from us to comprehend and be comprehended. We normally don't look like humans but since my birth I always have."

"How old are you?"

"About your age. I was formed to be a pair for you specifically. I was raised to have a stable ego with limited interconnection with the collective. The idea being I'd better be able to understand and communicate with you." the woman said then paused "I was made for you."

"That's a lot" Gia stated then decided to ignore red flags for now. "So others of your kind don't have stable egos?"

"Yeah for them self is very fluid and ever changing. A self appears for a purpose, even if that purpose is just to have fun, then dissolves into other selves when that purpose has ended. I've never experience this first hand. My purpose to exist is much longer lasting and I am very persistent toward that purpose."

"You said earlier your mind is based on humans. Is that because the collective formed you to be more like a human mind?"

"Actually yeah but I meant that we where designed and engineered by beings that considered themselves human and shared a lot of your DNA. In our timeline, a few centuries into mass extinction, humans discovered a way to create a symbiotic relationship between engineered fungus and human neurons. That lead eventually to a fungal human hybrid that could survive off much less and more diverse calories. Also they look basically human. They called themselves Mycans. But the planet continued to die in new, interesting and unpredicted ways.

"So after a time the Mycans struggled to survive and realized then couldn't live through the time needed for the planet to be healed. So they made us. Fungus was thriving in our dying world and we could survive and help redistribute things with cognizant long term planning. Unlike our unthinking cousins."

"So you have human neurons in your mycelium?" Gia asked.

"No they are modified, optimized and integrated into the cell structure by so much engineering there is very little DNA or structurally in common with your cells. It's more accurate to say we are sentient fungus created by humans and partly in their image. A good analogy is the AI beings in your dad's scifi novels." the woman paused "I guess except for me and those like me. Many of my structure are there to make my cognition much closer to yours."

This was a lot of lore all at once for Gia's mind to make up in the background. But still possible. Gia knew she had enough of an understanding to make all this up. *There is nothing knew here really.* Gia started to consider question to learn new information and fact check later. Then Gia looked up to see the woman across from her distraught, hurt even.

"I have an awareness of your surface thoughts. I was trying but they're hard to ignore. We're not really talking are we. Your testing me. And that should be fine. It's reasonable. I'm sorry I this is very hard for me." The conversations in the cafe behind Gia stopped. The part meadow, part city square fell still. The stars were still. "I don't even know why I'm going into all this detail. This was all stuff that was taught to me that I honestly don't find very interesting. I don't know how my brain works. Do you know the inner-workings of yours? I'm so alone and the only person I want to talk to only wants to focus on the nature of my existence. Which is like not my favorite subject. It's a lot for a first date." She paused "Shit."

It now felt late in the eternal twilight of the meadow. Conversation were started again in the cafe but fewer. The building across the way seemed sleepy and settled. The square was mostly empty. Yeah all social queues said it was getting late.

"I'm sorry" Gia started "Maybe we can start over. I'm not opposed to this being a date. I guess neither of us were up front with our intentions." Not a great apology but Gia realized that's not what she was trying to do. There is danger here whether or not this woman was real. Gia realized and tried to not think about it too hard.

"I'd like that. But it's getting late in real time. Time passes differently here. I can try to explain it later." the woman said starting to get up.

"Why don't we pick a time to talk again" Gia suggested.

"Tomorrow evening work for you. Say your bed at 6pm?"

Gia reflected on the various implications "It's a date."

"You'll be tired. It's a lot of work for us to link" she said leaving from Gia's view. "But don't worry I help you get good sleep" a bird said somewhere behind Gia.

The sun way setting but now only in one spot. *Shit I've been here all day without my phone or telling anyone where I was.* Gia walked back from the woods and braced herself for another hard conversation.

---

Gia didn't have good answers for "Where have you been?" or "Why didn't you...?" and so she lied. In the tale she spun, she didn't get any sleep last night. When the sun rose she walked into the woods and fell asleep. And now she had just woken up. Mostly believable and it was almost true. Gia had been back for an hour and was relieved that nobody had yet to suggest she should eat something.

"So what are some embarrassing stories about our Gia?" Ava asked.

"Yeah she never talks about living here. Give us that dirt." Valorie said. Valorie seemed excited at a potential shift toward a good time. *The day must have been so stressful with me missing* Gia thought. No one was blaming her or revealing how it must have been a big deal. *Why am I like this.*

"Well" Gia's mom, Catherine, started looking toward her husband. "She was really close with my Mom who lived with us for a time." A deep chill went down Gia's back. She tried to speak but there were no words and no one noticed.

"Oh yeah they were quite the duo. She watched Gia while Cathy and me both worked in town." Gia's dad, Tom, stated.

"That sounds really nice" Olivia said looking toward Gia now giving her room to speak. A silence persisted for a few seconds but Olivia did not look away from Gia so the silence held as eyes were on her. A twitch of doubt or awkwardness came from Olivia's face. She was about to look away and continue speaking.

*There's no way out but through.* "Yeah me and Gran were close toward the end. She died my first year of high school." *So much for a light conversation.*

"Yeah that was hard for us all. Especially Gia." Cathy said "Shortly after Gia told us she was a girl and wanted to transition."

"We had no idea about any of this stuff. We found the youth center in the city and it had a parents support group." Tom stated.

"I didn't know y'all went to a support group" Gia said.

"I guess it never came up. I think we actually started going to it more after you left when you were seventeen" Cathy paused. "But we were talking about how my Mom turned out to be much cooler than I ever knew her to be."

"Like she was an anti-war, free love, commie like you kids." Tom said clearly a fan. "And I guess we took up some of that after she was gone."

"Apparently" Cathy continued "Gia had told mom about wanting to be a girl but she didn't want us to know. Which fair I did vote republican back then. So mom and Gia went to thrift stores, dressed up and did all regular girl stuff."

"Oh rad. So what, she must have been one of those like 1930's radicals?" Ava asked.

"Yeah that would line up. We never found out all she got up to." Tom said "We learned all this stuff about her after she passed."

"That's some solid opsec if your daughter never realized anything." Valorie noted.

"Well she didn't raise me. I was adopted. We connected when I was an adult." Cathy said "I'm grateful for the time I had with her though. Grateful that she was there to help raise and support Gia in ways we didn't know how to yet. They played in the woods together a lot. Mom build Gia a tree house in this meadow in the back."

Olivia looked at Gia.

"She was a fit old woman right up until the end." Cathy added.

"What a legend. And you're paying it forward" Ava said.

Valorie touched the slight bulge of her tummy "It will be great to have y'all be a part of our little family. But also where's the embarrassing part?"

"Oh well I guess there isn't any" Cathy laughed.

"I don't know Gia's certainly all red" Olivia said.

"Oh honey did we embarrass you?"

"I'm fine. So did y'all do anything fun today besides worry about me?"

"Honestly it's bad but we didn't notice until like an hour before you stumbled out of the woods all dazed like" Ava said. "Me and your Mom went to this cool local museum and had a nice lunch."

"Val, Olivia and I went hiking. Each group thought you were with the other."

"Well that's nice. I'm glad y'all all had a good day. I'll be right back."

Gia got up and went to restroom. Then to the kitchen to get water. As she began to drink Olivia entered.

"They're all gonna play Root but I think I'mma sit it out."

"Yeah I don't think I'm up for a five hour board game right now either."

"Why didn't you ever tell me about your Gran?" *Fuck.* "She sounds a lot like mushroom girl." *Fuck, fuck.* "And she died right when the first conversation with her happened right?" *Fuck, fuck, fuck.* "Sorry I'm not trying to be" Olivia paused.

"Because it's all a lie." Gia admitted.

"Mushroom girl?"

"Gran." Gia said and Olivia waited. "My imaginary friend was real. And it was like I told you. Me and Gran were close but she never knew I wanted to be a girl. After Gran died I was really upset and then Mushroom girl talked to me. And that was terrifying. So I lost both of them at once. After a few months without my pretend world were I got to be a girl I couldn't stand it. So I told my parents I wanted to be a girl."

"That makes sense."

"I told my parents I had a friend who supported me. Then they wanted to know who it was. I thought if I said my imaginary friend who was in fact a fungus from the future then I'd never convince them to give me hormones. So I said Gran. She was died and I was the one who knew her the best. This whole story my parents told was a lie that came from me. And they believed it. And I got hormones."

"Do you plan to tell them all this at some point?"

"God no. My mom loves the idea that her Mom abandoned not because of a well to do family trying to keep things quiet but to bash the fash. My dad talks about her as his inspiration to join the occupy movement he got into after I left home. I spun a story about the past and it became real."

"All things equal, is it better to believe a happy delusion or miserable with the true?"

"If it doesn't cause harm then I'm for happiness."

"Is that what they would choose?"

"I don't know. Do you wanna cuddle?"

"Yeah" Olivia smiled know that there is a limit to hard conversation one can have.

## III
Gia woke up from a sound dreamless sleep. She was on the floor mattress. Valorie laid asleep next to her. After cuddling with Olivia they both decided to do there own thing for a while on separate beds. Gia figured they must of both been asleep when Ava and Valorie came to bed. I wonder how late they stayed up. The color of the morning light was still blue. I might be the first one up for once. Gia's stomach growled. And an awareness in her mouth told her she had forgot to brush her teeth.

Gia was considering the unusually good sleep she get when her mind drifted towards her dream about the bird. Then she looked at her left hand. Gia remembered there was a first aid kit in Valorie's Outback. She walked past the kitchen and outside. The car was unlocked and after only a moment Gia found a black medical glove. Useful for many things. Gia put it on her left hand.

Gia returned to the kitchen to see her mother at the stove.

"Do you want some eggs?" Cathy said. In bowl, she had cracked what looked to be a dozen.

"Sure" Gia smiled "I know y'all just went hiking yesterday. But I think I'm feeling like doing that today. I could use the protein."

"Well here have this." Cathy handed Gia a protein bar "There's no whey."

"No way." Gia joked and Cathy smiled. Cathy kept glancing over at Gia until she saw that Gia had taken a bite.

"So where do you plan to go hiking?" Cathy asked.

"Oh the rocky spot with all the cedar."

"They're junipers."

"If someone names a species of junipers mountain cedar then I get to call them cedars."

"I don't think they actually are mountain cedars though."

"Oh well that spot anyways."

"Kind of far out." Cathy said. *Exactly* Gia thought.

"It's a cool spot and I might never be by this way again. I want to see it one last time." Gia lied.

"Oh well can I come with. I'm feeling like a hike and spending some quality time with my daughter."

Gia nodded. "I wonder if anyone else will want to come." Gia said to herself. She ate 5 or 6 eggs of the scramble, 2 slices of toast, and the whole protein bar. Also she grabbed two more and put them in her bag. She put on her jacket from high school and some old sunglasses from the pile of things to sort through.

"So what's with the glove?" Cathy asked.

"Oh I cut my hand." Gia said ignoring her mom's concerned look. "It's fine. The bandage kept coming off from washing my hands" Gia lied. "It's easier to just leave on a glove. Something folks do it restaurants all the time."

"Yeah we call them finger condoms because normally you use them just one finger." Ava noted.

"Yeah it's just on my pinky. A finger condom would be nice."

---

Olivia, Ava, Gia and Cathy ended up going on the one hour drive to the spot that might have cedars. It wasn't that far but there were a lot of switchbacks involved in the route. Gia hoped it would be far enough. They took Valorie's four wheel drive Subaru outback. It was a great little bug out car and Gia loved driving it.

The park was quite big. It had a river cutting through it and several creeks. They were all standing in front of the park map.

"Let go here" Gia point to the opposite corner of the map as the *you are here* icon. "I want to check out the other side of the river. I've never been there" Gia said.

"Sounds like a plan Margret Ann" Ava said with one of her cute affects. Ava was wearing full hiking gear that she, no doubt, liberated from a store at some point. She was the tallest of their little family, fairly muscular, and still out classed five foot six Gia on femininity. When they first met all Gia felt towards her was envy. Then Gia discovered the feeling was mutual and sex sort of just happened as a happy accident of two girls totaling platonically playing video games.

Gia realized she had been staring and so kissed Ava. "Let us away" Gia said, quickly turning around, and the others followed. After a while they started walking two by two as the path narrowed. Olivia and Gia in the front and Ava and Cathy a bit behind.

"So what's across the river?" Olivia inquired.

"Hopeful a healthy ecosystem that is far enough away from home." Gia coyly said.

"That would be nice." Olivia paused to consider her next question. "What would be desirable about being far enough?"

"So mushroom girl. She exist in a limited physical area."

"Okay sure."

"And I have no clue how big that area is."

"Why not ask her?"

"Honestly she's being a bit scary at the moment and I wanted to try and learn more from another source."

"So like another mushroom person and maybe across that river on another mountain is far enough."

"I figured best to overshoot. If I ended up connecting with her she might be able to fool me into thinking she was someone new."

"Your really diving into the rabbit hole on this one huh."

"Well so far the stakes of going down this path is having a nice hike with people I love."

"That's true. But things can escalate quickly." Olivia warned.

"I do need help to not get too lost in the sauce. That's why your here." Gia smiled.

"I guess so."

---

Gia was eating her last protein while sitting in the shade with Olivia just off the main path. Behind them was a nice meadow I imagine a lot of folks visited. They ended up getting far ahead of Ava and Cathy. It was easy to forget that Cathy was getting older.

"So is this the spot?" Olivia asked.

"Yeah I bet that meadow would be a nice place of a picnic?"

"I mean to attempt communication with a new mycelial friend."

"Oh yeah. No this spot is good and probably best to do the deed when it's just me and you."

"I figure that was why you were walking so fast."

*Was I?* Gia thought. "Yeah I suppose so"

"So how does it work?"

Gia took off her glove and felt the cool, dry air. "Like this" Gia held up a left hand for drama effect then casually set it on the ground. And nothing happen. She then stuck her pinky in the soil.

"Well?"

"Nothing so far. Perhaps they're not around here." Gia shrugged. "I'll walk around the meadow and stick my pinky various bits of dirt. See if I have any luck."

"Cool. I think I'm gonna have a lay right here." Olivia face went completely neutral. *Classic Olivia meditation posture. Just strewn about on the ground. What a beautiful human.*

Gia began her search for connection.

---

In the late afternoon, they arrived back to the quaint, well maintained, double wide on her parents land. Gia was glad to be back. There are memories in everything here. Like the porch that ran the length of the mobile home. Originally much smaller, the expansion she remembered doing with her uncle. They still got on well after she transitioned. A lot of how she worked, she got from him.

Things changed though. They last talked at a family gathering ten years ago. Then things were still fine. The country changed though and her uncle with it. They haven't talked but he probably doesn't see her as an exception anymore. She wished for them to still be family. If they every did talk again what he'd might say terrified her. As she drove down the gravel drive, she saw his rotary tiller stored underneath the porch.

"Hey mom does that old tiller still work?"

"Oh uh, you know I have no clue. We took the gas out of it years ago and haven't used it since. Gave up on it when I last tried to garden. Thing kept shaking and cutting off."

"Oh the tape was probably getting to old to stay in place." Gia said thinking out loud.

"Why would loose tape make a tiller cut off" Olivia asked.

"Well without the tape the paper clip wouldn't keep making contact."

"And why-" Olivia started.

"Oh actually it is a bit of copper last I used it. The paperclip was getting too hot and made the tape start smoking after a while" Gia said smiling.

"Oh I see" Olivia said.

"Might be fun to look at later" Ava suggested. "Perhaps there is a means to repair it and reduce it's previous chances of exploding."

Gia smiled at Ava through the rear view mirror.

"That was all her uncle. Tom's brother. He liked to get-" Cathy paused "well rig things. Everything works well enough until the next repair was needed. Which was often the next time you'd use it."

"It like a living system. Maintenance and use go hand and hand. You could spent to long doing it right. But then you waste salvageable parts, forgot how to repair it years later, and it's less fun."

"Sure but explosions are bad." Ava said.

"Safety is a spectrum." Gia said half joking.

"Well I'll go ask your dad were the gas can is."

"Cool but don't bring it out or move it. Just show me where it is." Gia said exiting the car. A sudden panic ran over her as a breeze glided past her chilling her hand. She looked at her exposed left pinky and quickly curled it into a fist. Her right hand held the car keys, she quickly reopened the car door, the keys scratching the paint. This warranted a concerned look from her mother. "Actually I think I'll just go to the gas station. We passed a place with an ethanol free sign." Gia lied.

"Oh okay" Cathy said still confused. "Do you need an empty gas can?"

"I think we got one in the back." Gia lied and hoped was actually true. Valorie does tend to be prepared.

"Okay sweety."

"Can I come with?" Ava asked.

*I can't think of a single logical reason why not.* Gia thought. "Of course" she smiled.

"Cool well I got work stuff to do so I'll see y'all later." Olivia said exiting the vehicle.

Ava got into the front set. "It'll be cool to repair something together."

"For sure." Gia said as she rapidly searched for a no ethanol gas station that also sold gas cans.

"Need any help?" Ava asked.

"Nope just finding the address. I actually don't know these roads very well." Gia lied and started the GPS. As the car moved down the drive, she carefully found the rolled up glove in the side of the door where she left it and wrangled it onto her left hand. Ava didn't seem to notice the gloves absence then reappearance.

---

It was past sunset and well past 6pm when Gia finally made it back to her bed. Getting the tiller up and running turned out to be a wash. She and Ava still had fun though. There is a special type of cuddling that can occur when two people are working on a mechanical problem together. Things have been really hard and Gia had needed that.

She sat alone in her bed. Her hand was held in the air. She removed the glove and gently rested her pinky on the bed. Immediately reality fractured.

The walls on her bedroom crumbled with a loud rubbery squeak. Her bed melted into her and awareness of her body left as cold twisting mud conformed around her. She fell to the ground. Or rather into it. Like being underwater but underground. She tried breathing in a panic. Then she found herself deep in a hole. The act of breathing created air around her. A fire was in front of her now.

"Where were you?" the woman on the other side asked.

"I'm late but this is not okay." Gia asserted.

"This is what being here is like when I don't prioritize a gentle transition towards linking. Because we don't have time for that." She paused "But I'm sorry." Her face came into the light of the fire. It was deformed in a way Gia couldn't place.

"Okay. If we're in a hurry let's get to the point."

"Time moves differently once we're linked. We can spend a lot of time talking and it will likely only be seconds or minutes real time. This require a lot of energy but soon that won't matter."

"Okay so what's the emergency?"

"First I need you to trust me. And I recognize how hard that might be but here we have the equivalent of hours. Your life and those of everyone in your little found family depend on you hearing what I have to say."

"Threatening me isn't a good place to start."

"Fuck. Well I'm not the one doing it. You are in danger and I need you to trust me!" She paused "If I wanted to hurt you I wouldn't need to go through all this effort. In fact, all I'd have to do is nothing. Oh and I am in danger as well. In fact, I am the target. You're just collateral. Fortunately you have a body and we can talk. So you're the only entity in existence that can save me. But first I have to save you."

"Save me from what?"

"You talked to the mycellial collective today right?"

"No." Gia said unsure. "I mean I tried but nothing happened."

"They don't need to make you aware to get information from you"

*That's what that was. I thought I just lost time.* Gia thought.

"How much time did you lose?"

"15 or 30 minutes maybe"

"Great they know everything."

"How? I don't know anything."

"You know enough. Enough for me to be clearly a threat and not just some rogue ego they're laying siege too." The woman looked to be in great pain now "When the experiment was deemed a failure I was suppose to integrate with the collective. I disagreed. I thought there was no harm in waiting longer. So they have been trying to force my integration ever since."

"Fuck that's like. Why didn't you say that before?"

"I felt like today at about 6pm would have been a good time." She said bitterly. "I hoped with a day to reflect I might come off less scary. If you were scared of me and you knew I had enemies you might go to them. That happened anyways and now there is a wildfire burning its way straight through these woods."

"Wha-What? Why?"

"I have no idea. I imagine they see a human and rogue ego successfully communicating as a threat to their global plans to repair the ecosystem. Which I find rich. Every change they've made has only made things worse in this timeline. And since the whole going back in time thing was a one time deal. They are escalating to more desperate plans to saved a planet that they greatly accelerated towards death."

"Shit. Uh-"

"But I don't care about any of that bullshit. The planet won't be able to sustain our mycelium in a few hundred years and our gene bank will die and all hope of reversing mass extinction will end." She took a breathe. *Do mushrooms breathe?* Gia thought. The woman glared at her. "I don't need a few hundred years. I don't even plan for my ego to exist for very long past our last conversation."

"Why me? I don't know you! You might be able to read my mind but you don't know me. Your obsession isn't healthy. I'm open to being friends or more or whatever. But we have to exist independently for this to work." Gia said. She was done trying to have tact.

"But I do. I don't need to read your mind to know you. You don't remember. You blocked it all out. I thought-" she stopped herself. Then looked down for a while "I don't know how your memories work. I don't know how you don't remember certain things. Human neurology isn't something I understand. I'm supposed to be kind of like it but I'm not." Her face full of unspoken pain. "I am alone." The woman choked out.

Gia hugged the woman, no fire was between them anymore, a dim twilight glow surrounded their bodies. Gia did feel something familiar. There was a smell of something then it was gone. The woman moved and held her tighter. Gia knew that grip and realized this wasn't the first time they'd hugged.

The woman moved away slightly. The moment was over. She looked at Gia "We don't have time for me to help you remember. But I can give you my memories. It's dangerous. Integration is always a possibility with linking memories. But I need you to trust me."

"Okay."

Reality drifted away gently and so did Gia's body. There was nothing but endless oblivion. Then just in front of her Gia saw her refection in a mirror. But she was younger, maybe twelve, and also definitely a presenting as a girl.

"Hi Aimee." Her tween reflection smiled. "Wanna go fly on birds today?"

## IV
Aimee looked at Vanessa with some annoyance. "Birds again?" She asked. The little human girl nodded. At first interacting with little Vani was exciting. Aimee was so good at connecting. Her mother told her so. The first breakthroughs were images of interwoven mesh, and flowing lines of rhizome and tree structures. The reply from the entity later known as Vani was pantyhose on a human leg. Lipstick on human lips.

The next breakthrough came from Vani. A feeling of siting, two legs perfectly still, something full between them deflating. Aimee's mom searched their understanding of human physiology for this sensation. They couldn't find anything that match. This excited Aimee all the more. She was the most advanced of her peers in human communications. It seemed obvious that she'd discover some aspect of humanity that the collective knew nothing about.

It was around this time when Aimee receive that name. It paired with learning the entities name Vani. They related as best friends. Vani had imagined so much about Aimee and so she fit her representation into that image. Sometimes Vani would imagine them self as a boy and would go by Gio. Gio was impossible to get a response from. But Vani spent hours seeing and responding to images with images her own images.

This all excited Aimee's mom very much. She was proud of Aimee's progress. Aimee was so happy. The times where there was progress. It made the hard times bearable. Zanni wasn't like Aimee and Aimee's mother wasn't like her either. She was like a hybrid of whatever each of them were and alien to both. She was isolated from her peers to prevent contamination of methodology.

"Wouldn't we learn faster through collaboration?" Aimee suggested.

*There is value in taking ones time.* Her mother communicated.

"I am so alone. Mom please I need someone like me."

*You are not human. You do not need companions. Do not confuse desire for need. Do not let desire interfere with your purpose.*

*I hate my purpose.* Aimee thought.

*Do you wish to integrate and find a new purpose.* Her mother suggested.

"No." Aimee stated quickly. "I'm good at this. I will fulfill my purpose you'll see."

*I'm glad and I continue to be impressed by your progress.*

Aimee unlinked from her mother. Her one connection to the greater collective. She imagined they're all jerks like her mom. Humans were far more interesting. She decided she would rather be like a human. Her progress continued. *Gender exist and it seemed terrible. Biting finger nails was fun but toe nails were gross. Kissing felt nice.* These reports impressed her mother but Aimee stopped being interested in what her mother thought her purpose was. Humans decided their purpose and slowly changed to fulfill it. Aimee decided she'd try that out.

Aimee decided her purpose would be to help Vani. Which included her assigned purpose of establishing communication with Vani. So her mother was none the wiser. Actions towards the former could always be rationalized as the latter. Aimee discovered that Vani did have a big problem and she was already helping but Aimee could help more.

Gio was a lie that Vani's body told others. Aimee suggested that we only needed to imagine true things here. Vani responded with flying through the air magically. Aimee couldn't help Vani do that. So she countered with what she'd come to greatly regret, the feeling of riding on a giant bird. Vani loved this. Aimee figured this was at least possible. But she'd need to get access to the collective's gene bank. A problem for her future self.

"Vani is short for a name that you don't like" Aimee said.

"Yeah but I like Vani." She said looking down at Aimee's realistic sand ground.

"Why not make it short for another name" Aimee suggested.

"Like what?" Vani's face looked strange.

"Vanessa" Aimee suggested. Vani's face looked stranger now. Aimee feared she'd misstepped and done the opposite of her chosen purpose. It was stressful trying to live up to ideals you decide.

"Okay. I like that." Vani, now Vanessa, smiled.

*Success.* This emboldened Aimee. "And you told me about the difference between boy and girls right?"

"Yeah."

Vanessa made a strange face. *Keep going but tread lightly.*

"And you said that I was a girl and my name was Aimee."

"Yeah we're best friends."

"And best friends tell each other the true."

"Always." Vani asserted.

"I would say, based on how you explained it to me, that you are a girl." *Or just rip off the band aid. Why not.*

"I can't be." Vani said defeated.

*Okay let's just double down.* "You can't not be silly." Aimee playfully asserted and saw a positive feeling of some kind from Vani. "You are just stuck as a girl" Aimee held up a mirror "See?" And Vanessa saw herself as herself. "My best friend is a girl named Vanessa." Aimee asserted.

"My best friend is a girl named Aimee."

---

That success was huge toward helping Vanessa. There was so much progress toward communication also. Which kept Aimee's cover story in place. She was the best of her peers it would seem. Her mother's pride was nothing to her now. She had her purpose and that slowly made Aimee someone she wanted to be.

She looked at the maturing soon to be teenage Vanessa of her not quite accurate Albatross. "I'm bored with flying so much." Aimee whined.

"But it's so much fun."

"It doesn't get us anywhere. You've got to make progress. I want to help you make progress."

Vanessa shrugged. "I wanna fly."

"No bird until you make progress" the birds back shifts into a log on the sand. "You have to tell people who you are. They'll start to notice how I'm changing your body."

"Uhhh, no fun Aims. I-"

"My name is Aimee not Aims." She asserted. "And-"

"Well Aims is way cuter and suits you silly. And like you don't know what it's like with my Gran or my parents. And no one will notice because Gran's eyesight isn't the best and my parents are always at work. Plus I'll be out of here in a few years anyways. Once I turn 15. I know a guy and I can stay at his place."

"Vanessa I've told you I'm a part of these woods. I can't follow you if you leave."

"Oh Aims I'll come to visit." Vani said.

*Do I mean nothing to you.* Aimee thought.

"Oh don't be like that" Vani added.

"I thought we were best friends." Aimee said quietly.

"We are. But sometimes space is good. And plus I'm not trying to get away from you. It's my lame ass parents. I can't be a girl around them."

"You haven't tried." Aimee stated. " Communi-"

"It wouldn't go well." Vani interrupted. "Communication is like all you ever talk about. Your obsessed. We don't talk. We talk about talking."

"I don't understand the difference."

"Oh my god. Of course you wouldn't."

"Would you want to help me understand?" Aimee suggested.

"See there.You're doing it again. Like why do I have to teach you everything about talking. Like let's just talk."

"You have taught me everything I know about talking." Aimee stated.

"Oh you helpless, adorable dweeb." Vanessa said dismissively. She then tackled Aimee and they began to wrestle. Aimee had learned that Vani liked to conclude arguments with wrestling. It was in one of these sessions that they shared their first kiss, as practice, whatever that meant. Aimee did like it though. And secretly hoped it would happen again one of these times. It made the frustration of unresolved conflict redirected into wrestling more bearable.

They stopped when Aimee got Vani pinned against the log.

"Okay, okay you win." Vani said. "I'll write a letter. And think about giving it to Gran."

"That would be great progress." Aimee said not releasing Vani from against the log.

*Is this it?* Aimee thought.

"Okay Aims like let me up you silly." Aimee let her up and Vani brushed of the sand. "You know I think you're getting stronger."

"We're both growing up I guess. Your just growing up to be more feminine is all."

"Shut up. You goof."

"It's true. I know because I'm helping you be the girl you want to be. Pretty soon you won't stand a chance at wrestling." Aimee approached Vanessa.

"Aims"

Aimee smiled then held Vanessa as they kissed. Aimee held her tight.

---

"You named me Vanessa?" Gia asked stunned.

"You named me Aimee first. It seemed fair at the time."

"I know. It was. You were just trying to exist and find connection. So you're name is Aimee I suppose?"

"As much as yours is Vanessa. That was just the name of you're imaginary friend. My name can't be spoken in words" the woman in a casual tone.

"Okay but you need a name I can say."

"What would you suggest." the woman smiled.

"I agree with my past self. Aims does suit you. Also I'm glad you still look the same, just older. Your appearance came from me but I don't know it works for you."

"I feel like I made it my own." Aims said.

"Truly. So that's what a mind link is like huh. And that was all true. You were my first kiss."

"I will only ever be honest with you Gia."

"So then" Gia started "You said we're like maybe gonna die or something."

"It is a distinct possibility. But I have a plan."

"Okay let's here it."

"When you come back into real time it will be dark in your room. You'll be alone. Everyone will be gather in a huddle around a phone watching the news. Before you leave your room grab your bag and empty the contents. You won't need anything in there. Walk past the group and go straight into your parents room. Then...."

As Gia listened to Aims' plan, she realized the intensity that scared her before came from the fear of someone how's life hangs in such a delicate balance. For Aims planning was safety from routine mortal danger. Gia realized that this was all actually her fault but sense no resentment from Aims. There was a chance that Gia might die tonight. It was very different for Aims. For her there was a chance that she might live. Aims concluded her briefing.

"So what do you think?" Aims asked.

"I think it's really well thought out. And Aims I'm not going anywhere without you. Until you can actually survive without my help. I am going to be here for you."

"That makes me happy but-"

"Aims." Gia interrupted "I've decided that my purpose is to help you." Gia kissed Aims on the cheek. Then on the lips. "Okay I'm ready."

"I'll see you soon."

---

Gia snapped into reality in a dark room. She was alone. She put back her glove. And emptied her bag onto the floor as she left the bedroom. She walked quickly and silently pasted the group, of almost everyone she loved, transfixed on a phone screen.

"-the fire, of unknown origin, was first reported earlier this evening. Local departments in the area have rushed in from neighboring counties for this unseasonable wildfire-" the phone blared then became inaudible as she gently closed her parents door. She began packaging the items Aims had indicated. Everything was just where she said it was. *Her mycelium really is just all over.*

Gia exited her parents room without notice and out the front door. The words "Out of state mutual aid" came from the news report. This peaked her interest but she had to focus. She placed the bag in the outback. Moved out unnecessary items into the drive. And setup and almost secured the rear facing seating Valorie custom made.

Gia sniffed the air. Then coughed. *Sh-shit, shit.* She barged back through the front door. Now was the time to be noticed and she was.

"Valorie, Ava, Olivia get your bags from the room and meet me by the car. Mom can you get two jugs of water and six kitchen rags from the back room. Medical mask if you have any. Dad I need your help getting the car ready." Everyone just looked at her for a moment. This was good, everyone needed a moment to let it set in. Gia notice Olivia was about to say something. "Let's go now." Gia said in a tone only a few of them had even heard. And they did.

"What's wrong with the car?" Her dad asked as they exited the front door.

"I can't seem to get the rear facing seat secured." Gia lied. The trunk was open and he went right there. Gia took a moment to glance at that old tiller one last time. A click from behind her.

"Got it." Tom said.

Gia went to the driver seat and said "Help me find the emergency AM radio station." Gia turned the car on and heard figures with luggage emerge from the old double wide. Her dad sat next to her in the passenger seat.

"Found it" He said triumphantly. The updates on the fire began droning out. Gia turned it down slightly. He was quick. Time for the hard sale.

"Awesome!" She smiled. "Dad" She started looking into his eyes. "We need to take one vehicle. It's better if we stick together." Gia said technically not lying.

"Sweetly me and your mom will take our car. We don't want to lose it in the fire." He argued. *Your eyes. You can't see well at night.* Gia thought knowing she couldn't say that.

"You have insurance right? It will be covered if the fire gets here." *When the fire gets here.* "I need you and mom with me right now." Gia said beginning to tear up but not for the reasons she stated.

He sat thought for a moment as everyone got into the car. Olivia was in the back rear facing seat. Bags were across people's laps. "Okay." He said finally. The final door closed. Gia set the air to recycle.

"Mom give everyone a wet face cover." Gia said and saw her mom nod through the rear view. The car moved fast down the gravel drive. Gia turned left instead of right.

"Honey our turn was the other way." Tom said. Gia was accelerating rapidly now on the asphalt road.

"We not going into town. We need to cross the nearest stream. Plus I think the fire is coming from that direction."

"The fire is in town. The news hasn't reported on that." Valorie said.

"No it's not that big yet. But it's close to us." Gia said. *Shit was that too much to say. Can't really explain that a sentient fungus told her exactly where the fire was.*

No one said anything else for a while. Behind them there was light on the horizon. Like a sun rising from the North.

---

Hitting traffic on the highway was the first moment Gia allowed herself to relax. She became aware of the tightness in her stomach then. The others were alarmed by the sudden lack of mobility but there was no way the fire would make it this far. Aims had explained. And the fire response would prioritize protecting this endless seeming convoy. *We're safe.* Gia thought as a helicopter thundered over head. This unsettled the car.

Everyone was talking and listening to the play by play of the emergency broadcast. There was no service in the convoy. Gia remained in her thoughts and went over the next steps. Phase one of the plan was complete. They survived. In the morning phase two will start. Protect Aims from assimilation.

Her body existed entirely underground. She would survive the fire. The trees and other flora that was her energy source would not. Before the fire reached her, she dumped as much energy as possible into several reserves and decoys. That's how she managed to communicate so much to me so fast. She was actively killing all the flora she had access to.

These reserves wouldn't last for long and her defenses would break down. That's where Gia came in. She hoped she was up to the task. She would find a way to make it work. She wasn't going to let Aims get killed. They were a pair. Their fates were tied together. They always had been. Gia was done running away from her purpose.

## V
It had been three days since Gia kissed Aims goodbye for what might have been a final time. In the hours of sleepless nights since, she has thought a lot about that moment. It felt right, Gia had thought, even though it was all based on assumptions. While Aims is definitely 
real, fires just don't happen out season suddenly, and seemingly targeting your house, there's no reason to believe that she was being honest.

Perhaps what she showed was mostly true or even entirely true but carefully curated. It was exactly what Gia needed to see to follow Aims' plan. Which did save her, and everyone she loves. And it was more than just saving our lives. Telling Gia to pack her mom and dad's favorite work clothes, nice boots, and good gloves was thoughtful sure. They were very happy to have clean, well fitting clothes to do all that work we've all been doing that last few days.

But Aims also had her grab her dad's mom's mom's watch and her dad's dad's revolver. I had hardly ever seen these things. But Aims knew them to be important. Not needed but important. For my mom it was the seeds said saved for many seasons and some old photos she hadn't digitized. Also a box full of papers Gia didn't look through. They looked like letters and trinkets though. And finally there government papers, that weren't all in one spot. Luckily Aims knew where each was and had their plastic safes combo. Even had Gia practice it in mushroom space, as Gia started calling it.

Her parents were stunned by what she had packed. Happy but stunned.

"It was your house at risk. So my adrenaline brain prioritize packing things that looks necessary." Gia lied. "I just grabbed what I looked important. And what I knew was important."

"But how did you-" her dad started.

"I don't know." Gia interrupted. "I was going off intuition mostly. I hardly remember my reasoning. And I was-" Gia paused thinking of her next words carefully.

"Thank you sweety. I don't know how you did what you did." her mom said. "I am so grateful and impressed with the woman you've become. And we're grateful you are in our lives again." There was a bit of crying after that and fortunately no more follow up questions.

Gia wasn't the woman her mom was impressed by. That was Aims. *Why can't I remember anything to confirm what Aims showed me of our childhoods?* Gia wanted to believe that everything Aims said and showed had been true. But why haven't they linked since the fire? Gia went to the spot Aims had indicated. Then went to a bunch of different spots. No Aims. Maybe Aims was good but she wasn't honest. Her plan involved so much deception for Gia to carry out. Deception must be a tool Aims has used to survive. Gia had to trust Aims despite that. Aims needed her. She must be so hurt from so much suffering and struggle. And hurt people hurt people. Gia would help but needed to make sure she didn't get hurt in the process.

*So yeah maybe kissing her was mistake.* Gia cringed.

---

Today was the last day they were going to spend cleaning up the land and mucking the remains of the various structures. They were all staying in a wall tent Tom used for SCA, society of creative anachronism. Their was at least one society of creative anarchism joke when Tom explained what SCA was. The wall tent had a furnace, a tarp and rug for flooring, and three airbeds from Walmart. It was cozy at first. After five days it was cramped.

Today was the day us four girls were suppose to head back after a lovely week getting to know two of our future child's grandparents. And it was almost that. There wasn't really a reason to change that departure date. Except now my parents were coming with us. And were going to rent a nearby apartment, instead of renting a house. The land wasn't going to be sold anytime soon so Gia's parents decided to downsize their plans.

Gia was still trying to figure out how she could rationalize her staying longer. She was still searching for Aims after all. She had to trust Aims had a plan and had a reason to not say all of it. So Gia decided to trust her instincts and hoped that Aims knew her well enough to predict Gia's actions.

Gia just needed some reason why she wanted to stay. She figured the truth was the best course of action. The best cover stories after all are true ones.

---

"I don't understand. You want to stay here longer." Valorie asked voicing the groups collective confusion.

"Yes. There are ways to speed up regrowth after a wildfire. And there are key areas to plant trees and ground cover to prevent erosion. I've been researching and talking with the state university. They think they can source free native plants for the project."

"How long is all this going to take?" Cathy asked.

"At least a year but-"

"No way. We have a child coming we need you at home Gia." Olivia stated.

"But you don't. I can visit and be around of course. I don't have a job right now and I wasn't really going to be that useful with infant care. Plus without me there you can give my private room to my parents."

"Gia what is this we're getting an apartment. Staying with y'all is just temporary." Tom said.

"It doesn't have to be. And that apartment is hardly cheaper than the house y'all were going to rent. Y'all need to save money. Live with us. I'm sure we'd be happy to have you." Gia said hoping she didn't just over step.

"I like the plan." Ava said breaking the silence. "We've never cared for an infant. They have. Our little Gia. It would be nice to have someone with experience to defer to." Ava said and smiled at Cathy.

"I would like seeing the baby more and Tom you know you were planning to be there watching the baby as often as you could." Cathy said. Tom shrugged in a noncommittal agreement.

"Are we actually considering this?" Olivia asked. The lack of immediate response being enough of an answer. "Gia you can't live in a wall tent for a year."

"Maybe more. Might take longer than a year." Gia interrupted.

"You can't. We need you and it's going to be hard out here all alone. You haven't ever lived alone really." Olivia said.

*I won't be alone.* Gia thought.

"We can visit then." Ava said. "Starting with me. I'll stay with you Gia. Help you get setup. I can take a plane or bus back."

"Or we can just switch off with the car." Valorie said. "It's a short drive really and when someone comes to get Ava. Ava can leave in the car and the driver can stay."

"I don't know guys that sounds like a lot." Gia said. "But yes I'd love to have you here for a bit while I get started." Gia said to Ava.

"This is a lot." Cathy emphasized "I mean you're welcome to be here. And if what you're saying is true it might mean we can sell the land sooner than later. Are you sure sweety?"

"Absolutely. I grew up in these woods. They mean so much to me. I want to help them rebuild. I might be the most qualified human on the planet to help restore this little patch of woods. No one can care for it like I can. If can do this" Gia paused and looked down. Everyone's face suddenly became too overwhelming. "I can be the parent I want to be. Because right now I'm not sure. And I want to be sure."

There was a lot everyone wanted to say to that. Gia saw it all over their faces. But no one did. They understood at some level how painful this was. If this conversation was one on one Gia was sure it would have continued into ration arguments, reassurances, curiosity, understanding. But there was something silencing about a group meeting. A strong lack of consensus can sometimes be a lesser form of it.

Ava hugged Gia and the conversation concluded. There were other things for the group to discuss but we all just play Root instead. All the while Olivia kept glancing with concerned looks. Gia just had to stay with the group until Olivia left. Then phase two could continue. Or perhaps phase one and a half: find Aims.

---

Everyone stayed one extra day to adjust to the new plan. A bike was gotten out of a storage unit. Valorie wanted to help Ava fashion a basket to said bike and this little project quickly spiral into a cart for the bike plus a basket. Ava and Valorie together can be like a whirlwind of productivity. Gia helped of course. Mostly to avoid being alone with Olivia. When not with Valorie and Ava, Gia found herself with her parents who she would miss a great deal. It was a lot of work, ensuring she didn't have a moment of alone time that Olivia would notice.

Olivia was being very diligent but respectful. She never once asked for a word alone until they were just about to leave. The car was packed. Just enough room for four people. Gia couldn't imagine a reason to deny a loved one a private goodbye. They walked together up the hill away from the a car that was all ready to start the long drive. It was morning. The sun was just over the horizon. Without trees it did that much earlier.

"So your reason for staying, is that the full story?" Olivia asked with no time or desire for subtlety.

"No." Gia said. *Honesty is best route here.* "I want to repair these woods and I do think it will be a good project. But yeah I haven't talked to mushroom girl since the fire. And I'm trying to. No luck though."

"The other day the fire Marshall wanted to talk to you privately. What did he ask you?"

"They're looking for suspects. As you know, they believe it was targeted arson. They don't know why or how though. He asked me not to share this. But like fuck that. The fire had multiple points of origin. Our property was definitely the target. And it would have taken multiple people."

"Why tell just you all this?"

"They think one of the arsonists might known me. Like a targeted hate crime type thing. The fire obviously got way out of hand. But that is the running theory they shared with me."

"Gia if that's true they might comeback. You can't stay here. Does Ava even know the risk?"

*How can I say that the risks are over. The collective did their play. They can't do a second fire.* "Your right I will tell her now. Let her decided if she wants to stay."

"No this should be a group discussion. No one knew about this. It's not safe. Gia this has gotten out of hand. This is dangerous. I'm doing what you asked. You can't stay here. I won't tell anyone your true reasons but you have to listen to me. If someone did this because we're trans. What would stop them from coming back?"

"Easy. They got away with it. If they get caught then they go to prison for a long time. The damage from their arson is like lifetime sentence situation. No one would risk that. Just to what, get us out."

"Gia. You can't be sure. Why risk it?"

"How about dignity? Fuck them." Gia shouted as tears blurred her vision. "We aren't leaving because of them. This was my home. This is where I'm from. They might be able to force me out. Sure. But I'm going to give them a fight."

"Fine. Then I'm staying too."

*Fuck.* "No have your job Olivia."

"I'll figure it out. You take priority. I'm not losing you. So if you're staying well fine. I am too."

From down the hill it was clear that their conversation was no longer private. Sound travels in these longer woods. Olivia became aware how loud they both had been speaking.

Quieter now Olivia continued "You're staying, I'm staying. And so is Tom's gun. Deal?"

"Okay. But if you're here. Then you're here. You can't wear me down trying to convince me to leave. I can't handle that."

"Okay. We'll talk to Ava. The three of us. We'll all know the risk and agree to a plan. Then if that plan is to stay. I'll stick to it."

"Fine. I can live with that."

They walked down the hill together. About halfway to the group they started holding hands. Gia leaned on her shoulder. By the time they got to the group Gia realized that she needed Olivia of course. Olivia was so important to her.

---

Gia looked out from atop the hill at the setting sun. It looked like by the solstice the sun would be setting behind their tent. Smoke from the charcoal and wood that had survived the wildfire came from the little metal chimney. With three people, the tent became cozy again. She had taken to sitting up here, when the weather permitted, at the warmest part of the day.

It was an unseasonably warm today as the temperature still felt nice at sunset. Soon it would get very cold. But Gia felt like lingering and seeing how long this bit of warmth would last.

"You've done so well." A voice said seeming to warm the air around her.

"Aims?" Gia said quietly. Hope can be such a terrible thing.

The sun's glow extended throughout the horizon. The air became temperate. "I don't have much energy so this conversation may take a lot of real time." Aims said.

"It will get cold then. I should move my body."

"You're actually in bed silly. You look to be sleeping. Unusual for you to fall asleep early. You must needed it perhaps. The other two won't try to wake you. Their voice are whispers."

"So you made it to the tent?"

"Yeah a while ago. I make and break the connection to check in. The collective has spread all over. They get information from you. You'll need to be very diligent about wearing a glove from now on."

"Of course."

"So I'm living in the septic tank. I've actually been here for a long time. I hid that detail from you and that turn out to be vital. The septic system was my original route into the house. From there, the floorboards. But I've sealed myself off and disguised that seal. I should be safe but I will run out of energy. We need to form a plan but it will take time."

"Sure. Yeah."

"It's 4am now. I will put you to sleep. We will talk tomorrow night."

Gia awoke late that morning. She sat up.

"Wow good to see you up. You slept for like so long." Ava said.

"How long?"

"Like 14 hours."

"Dang." Gia yawned. "I think I want to build a compost bin. Somewhere nearby."

"Sounds fun." Ava said.

## VI
*I want to tell you something but it can't be explained and believed with just words. So instead I intend to show you. But you have to promise me. You can't try to explain this to anyone else. Trust me. They won't believe you. They have to be shown. But before I can show you you have to consent to being shown. The truth is awful. Some might prefer not knowing. Do you want to be shown?*

"What is this? Where am I?" Ava said into a void.

*Think of it like a dream. That's all this has to be. A weird, perhaps unsettling, dream. Your mind will struggle if you choose to continue. You will feel very tired. I ask that you consider your chose as if this dream is real. If you choose to continue there will be consequences but also truth. You can reject the truth and this will stay as just a dream.*

"So a matrix situation? Well obvious the truth. I've all ready come out. Oh god, am I actually cis?"

*Please consider carefully. I will ask again one final time, do you want to be shown?*

Ava without hesitation said. "Show me the truth then."

---

Ava woke up. She was sweating. It was very hot. She panicked. *The furnace must have-* She thought as she looked over and saw no uncontrollable fire. It didn't even seem to be lit. But she felt very hot. The tent had no one else in it. She took off her nest of blankets. She worn nothing but a tank-top and feminine briefs. Her skin was bruised and layered with dirt from scant showers.

Then Gia and a woman she didn't know entered the tent. Ava threw a thin sheet over herself. *Why was it so hot?* Ava thought.

"Hey you." Gia said. "I have someone I wanted you to meet."

"Sure uh-what time is it?" Ava asked.

"Technically night time." the woman said stepping forward from beside Gia. And at second glance Ava realized she was incredibly traditionally attractive.

"That can't be right. It's light out and like really warm." Ava responded.

"Would you like it to be cooler?" the woman asked. And then it was. The light from outside the tent dimmed too. It seemed like maybe close to dusk now. *What the fuck is happening.*

Gia stepped forward. "Don't worry you are safe. It's me Gia." She stepped closer and Ava flinched. "We can explain everything? You asked to be shown the true so that is what we're doing."

"How do you know about that dream? Shit I'm still dreaming aren't I."

"That's possible. So just come along for the ride. After you see somethings I'm sure you'll feel better." Gia reassured.

"Okay. I'm game."

Ava then watched as the tent doors opened to reveal they were on a beach. The setting sun was blocked by one of the tent's walls. Blocked by the opposite wall their seemed to be a bonfire. Just out of sight. It gave the lighting looking through tent's opening a symmetry. 

"This place you've imagine is quite beautiful isn't it." Gia said.

"What do you mean?"

"If this is a dream this all came from you." Gia suggested. "But there is a second possibility, that this isn't a dream but still a place that came from your imagination."

"It is nice here. Okay so if this is not a dream then what is going on?"

"First let's get out of this stuffy tent. I want to watch the waves as the moon rises. It's a full moon tonight. Lucky us." Gia said and start out of the tent.

"Wait I need to find my clothes and get dressed." Ava said in a mild panic.

"Those clothes would be too warm here. Plus my friend won't mind. I think you look real cute in what you're wearing." Gia said and gave an uncharacteristically confident wink.

Ava blushed. "Uh-su-sure." Considering for a moment before getting up, Ava followed them out of the tent. Gia and the ephemeral goddess whose hair shimmered slightly from the bonfire. They sat near it but faced away toward the ocean. Ava joined them nervously. The woman was between her and Gia. The temperature was perfect with the ocean breeze and the fire at their backs. The three sat on a blanket that with just big enough to hold all of them. Ava became aware the skin of her leg and the woman's were touching.

"Living is so hard and painful" the woman started. "But this moment right now is really nice. Thank you for sharing it with us Ava."

"Sure. So uh what's your preferred name?"

"Aims. She/her." She smiled.

"A pleasure. She/they for me."

"I know. Gia has told me a bit about you." Aims turned to face Ava. There faces were so close. Ava was faintly aware Aims had placed a hand on her leg. "How would you like to help us save the world?"

*What the fuck.* Ava thought as she looked into Aims' eyes. Deep dark brown. Impossible to read. But a look are her face that could only indicate mutual desire. "Well of course. I- I want it to be saved. But I'm just one person. Best I could do is blow up a pipeline."

"A good start" she said. Her lips begged Ava to kiss them. "But we can manage so much more once we start admitting to ourselves. I want to save the world. Don't you Ava?"

"For sure." Ava said. Desire wasn't the right word for it. This went beyond that. Ava realized Aims' hand had found it's way up her leg and a part of Ava had found it's way out of her briefs. "But what-" Ava said with a dim memory of what words were. Ava was on her back now. Aims' hair in Ava face.

They kissed. "Was that okay?" She asked innocently. Ava grabbed her and they found their positions reversed.

"Who are you?" Ava asked. Their bodies intertwined moving as one.

"Gia's friend and I think maybe yours. Are you ready to see the truth?"

Ava became certain that this was no dream. "Yes. I'm ready to know whatever is going on here."

"As you wish. It's a pity I was hoping that would continue a while longer" Aims said.

"Perhaps later it will." Ava held by her deep gaze.

"I'd like that."

---

The three sat around a table next to the bonfire. A circular woven bench match the tables height and encircled them. They sat on cushions laid on the floor. They backs against the bench.

"And so that's why we actually stayed out on my parent's land after the wildfire." Gia concluded.

Ava had mostly listened up until this point. Only asking short clarifying question here and there. "So if your kind messed up our timeline, how was it supposed to go?"

"They weakened nuclear energy to make room for space based solar power to take off in the 1970's. Instead fossil fuels filled that power vacuum the collective created before the tech was possible. In our timeline. Fossil fuels fell out of fashion with the rise of nuclear."

"But you said there was still climate change in your timeline. Still a mass extinction event."

"Yes. The planet was still harmed. Mass extinction is a spiral. But for centuries the planet was made habitable with great effort by my progenitors. Then for thousand of years for my kind. But alas the planet could not be saved. Nor could we leave it. Our extinction was imminent. So we did something horrible to give earth life a second shot. This action is something the collective vowed never repeat. Measures where put in place to ensure it wouldn't happen again."

"You're talking about the time travel."

"Yes. Time does not branch. Nor go backwards. They found a way, that they intentionally forgot, to reverse causation exactly. If life does exist elsewhere in the universe then our actions killed them. Reseeding the timeline. All those potential life forms that might exist made to not exist and replaced with their ancestors that would birth different children. A potentially infinite genocide."

"I don't know if I agree with that conclusion. I mean-"

"Let's not get caught in the morality of universal time travel. The point is the collective can't do it again because they intentionally forgot it. And I sure as hell can't figure that out so that moral debate is hypothetical. We have to focus on the situation we're in. Not the possibilities lost to the past."

"Okay. You're right. So things are worse now right. How long do we got?" Ava asked.

"For humans. About what will be your remaining natural lifespan. For us 300-400 years." Aims said.

"No way that can't be right even our most pessimistic model don't-"

"Our model and understanding of mass extinction are far more advanced. If you want I can try to give you the basics. But honestly I was never that interested in this stuff. My focus has always been on human communication."

"Oh jeez. It's just hitting me again. This is real. Like really real. I'm talking to someone from the future."

"Yeah this is fucked right. It's why I ran away from home at 17. I blocked all this out for almost half my life." Gia said. "I think your doing great. All things considered."

"So are you actually schizophrenic or was that all just sentient mushrooms tripping out your brain."

"I am schizo yeah. It makes my brain easier to connect with hers." Gia explained. "To make you compatible you need to use LSD."

"Uh what now? So I'm literally tripping. Right now. You have me acid?"

"It's a micro dose given through a neuralink she created by engineering some cells in your cervical spine." Gia said.

"Right so you drugged me." Ava asserted.

"In my defense I did ask first. I said I got acid from a high school connect and we were gonna trip on the solstice remember?"

"Yeah. I remember that."

"It is in fact the solstice. But the tabs were fake. Aims delivered the drug. If you'd taken the blue pill, as it were, we would have just had a fun night tripping together." Gia reassured.

"Okay so I'm tripping right now right?"

"Eh. The dose needed is actually pretty small. You're basically sober."

"Okay let's go then. I want to exit from here and just talk to you Gia."

"Of course. You're never stuck here all you have to do is ask and then-"

---

Gia awoke in real space. She watched the stars dance in the sky. Ava laid next to her. They cuddled under a blanket near the furnace on an air mattress. The tent door open. Olivia had left a few days earlier. Tomorrow the two of them will return home for their first visit home for the holidays.

"So that's the real reason we're out her." Ava asked.

"Yes."

"So it started as just trying to say goodbye to your childhood imaginary friend and now what that has become save the world."

"I mean wasn't that why we've risked so much already. The actions and that. Wasn't that the hope. We are nature defending itself."

"And so your serious about that plan? I mean this would be a huge escalation compared to what we've done previously."

"I want to live in a world our son can grow old in."

"It seems too late."

"Yeah." Gia agreed "but we have to try. Right?"

---

They woke up to Valorie pulling up right to their tent. For comic effect Gia thought. Through the front window Valorie waved excitedly then exited the vehicle. They had packed before taking the tabs and so after spending an hour of the three breaking down the tent, folding, and packing it. Gia was behind the wheel. And ready to be back home.

Valorie had so much to tell them about all the plans my parents and her had gotten up to. Gia was really happy to have such a warm place to return to after working so hard in the cold. Gia knew she'd have to return in the new year but she planned to enjoy the right now. They were well in the drive now.

Valorie turned down here music slightly. "So how's repairing your childhood ecosystem?"

"Cold" Gia said. "Hard but worth the effort. I'm learning so much. Someone from the university came to check out what we were up to. They're a grad student whose thesis is on wildfires. Man made and otherwise. Mostly on the after part."

"Yeah Olivia was telling me about that. She seems much more on board than when I left."

"Yeah I never thought we'd get our inside girl to live outside with us." Ava remarked.

"She's been adapting well." Gia noted.

"I think about that summer we spent in those hammocks. Waking up by and getting to know our neighboring birds songs. The activities of the squirrels. The cute slugs and great caterpillars. I miss that so much." Valorie said holding her tummy that was now showing on a second or third glance.

"We can still do those things Val." Ava said.

"Maybe. But not with a child. Lately I've been wondering-" Valorie was starting to tear up. "Sorry I wasn't planning to talk about this."

"It's okay. I'd want to hear if you felt like sharing." Gia said.

"I feel like that child that we're having isn't real to you yet. And he feels so real to me."

"Maybe your right." Gia admitted. "Can that be okay?"

"Will you be back. Will things be like we planned. Were we try to raise a child well. And adopt more and foster. Make a village. Do you still want that?"

"I do. We both do. We talk about it all the time when we're out there." Ava said.

"I couldn't do what I do without you helping making that hope a reality Valorie."

"Promise you'll come back when it's time. Promise you'll be in our lives."

"Valorie you know that I will if I can. I can't control what happen if the state kidnaps me."

"I need you to be around too."

"The world needs to be around for our son."

"You're not going to having the planet Gia."

"I want to." Gia whispered. "I want to save the planet Valorie."

"More then our son?"

"Yes. Valorie our son won't be able to live in a world without people defending it."

"Maybe. But why you then? We did our part. We so much. Let someone with a child do what's need." Valorie said.

"We can't be choosy with who." Ava finally weighed in. "No one is right for this task Val. We're all imperfect for it. All risking serious consequence. If only people with nothing to lose did actions then nothing good would ever get done."

"Your talking like- fuck. You are planning something." Valorie realized. "Ava, Gia wake up we are about to raise a baby together."

There was silence for a while after that. There wasn't much left to say. They all had divulged things they were avoiding saying. They wanted to not ruin what was a break from all the stress. It was lucky that there was still a ways to go. Time to sit silently and think. When they got to the house that Valorie's parents had left her, they three just sat together for a while. Valorie sat in the middle managing to rest her body on both of them.

She was looking off into a corner of the room when she final said. "Okay." And so it was.

---

They weren't really celebrating Christmas but they also weren't celebrating something cool like Kwanzaa. It was just the holidays; they all agreed. Starting a new tradition as new families do, steeped in their culture and various subcultures. It could easy be called Christmas, their was a tree with ornaments, but no one did. Just above the tension and self consciousness, there was confidence from nostalgia and sharing stories and memories. A holiday most relaxing when intimacy and repression were in balance Gia thought.

Friends and family were invited to stop by as various post Christmas gatherings. By the 27th things felt calm. An old friend of Ava's was over and they were drinking and playing the quick board/card games meant for such times.

He was a cute. Ava's old friends, Daniel. He dressed, talked and smiled like some idealistic farm boy. In his mid twenties, he was still coming out of his acne. He was fit and looked like he could carry two 50 lb bags of beans over each arm for hours. But also wouldn't know what to do it a fight. That was just his vibe though. Gia also felt like he was hiding something. A fun guy for sure.

And I mean her probably was of course. The guy talked vaguely about things and stories without names. In such a casual way. Also works in information security. And friend's with Ava. It can be nice to meet a colleague. And just talk about nothing important. Each having full empathy about the others strategic vagueness. No self consciousness about say the wrong thing. Because the only true social plunder is increasing security or safety risk. So they could just talk as humans.

"So Olivia has been telling me about Rust. It seems neat. By the way she talks about it." Gia said.

Danny shrugged "It does make big claims. But-"

"Okay" Gia interrupted. "Marry, fuck, kill. Python, C, Rust."

"Well with that list." He paused and smiled. He clearly already knew his answer. This was a part of his affect. He wanted to look kind of dumb. Gia admired the effort. "Okay well. I guess marry C, fuck python. Sweetly though you know. Python deserves a good time. Then with a heavy heart, there is no choice after all, kill Rust."

"That makes sense" Olivia said trying to reduce perceived tension that wasn't actually there.

"What would yours be? from that list" Danny asked Olivia with the most genuine seeming smile.

Olivia started always done for a bit. "Well marry Rust obviously. Fuck C and have a truly magically experience but she's to set in her ways to settle down with. And kill python. It was great but we don't need it anymore."

"Kill Python?" Danny seemed shocked, or was he actually?

"There was no other choice." Olivia smiled.

"Do you have any idea what they're talking about?" Ava asked toward Valorie and Gia's parents.

"I mean I know python a bit. Like I've modified a script or two. I was always told that if you learn just one language then it should be python." Valorie said.

"That is true for now. But that advice only holds for people that are adjacent to software develop. Rust aims to be a language for everyone, and for everything. There's no reason we need like 8 different core languages do all of our computing." Olivia said.

"See this is why folks are so hard on Rust." Danny laughed.

"Let them. We can take it." They both smiled at each other.

"Is this what you kids talk about nowadays?" Cathy asked.

"Mom we're all like 30." Gia said

"Yeah I'm the baby here." Danny added. "So I can confidently say. No the kids are talking about way cooler stuff."

Tom placed down a card and the game was over.

"Well I'm going sit this next few out" Ava said as the fluxx like homebrew game they found in an abandon theater suddenly ended.

"It was just getting good." Tom said.

"I might have a smoke." Danny said glancing at Gia then got up.

"I'm down for another." Olivia said.

"That's the spirit."

"I might go get some air too" Gia said. "It's a bit stuffy in here."

It was a bit breezy in their backyard. Gia shivered. Then took a breathe and put her arm around Ava for warmth. Daniel lit a cigarette. Offered then put the pack in his shirt pocket. He seemed not cold at all in his tucked in long sleeve plaid button up and jeans. Gia noticed around his waist a slight waist line and white inside his sleeve. Then it clicked he was wearing long johns. Not unusual so this time of year but meaningful. He planned to talk outside during his short visit.

"Grey skies have always made me feel at peace." Danny said puffing the cigarette, pushing smoke out his nose, but not inhaling. If he had buttoned up to his collar, it would be less obvious. "Like now is the time to make the big preparations for the spring but there is also no rush. The work isn't on the seasons schedule yet."

"I prefer the fall." Gia said engaging in the small talk to get a read on what they were really doing here. "But I don't harvest anything. I imagine for you the fall is a lot of work."

"For my parents." Danny offered. *Why so vulnerable?* "I don't do much farming except to help out." He admitted. *What a wonderful cover story.*

"I imagine they need help at various points in the year." Gia said "It must be hard having to put your life on hold for months at a time."

They shared a look of mutual recognition. "It can be yeah." He agreed. "Ava told me about Aims."

*Well shit.* "Are you-" Gia started.

"She didn't say much" he said with a friendly look towards Ava. "Just I'd need to be shown. And whatever it is. I'd like to see the truth."

"Are you sure?" Gia cautioned.

"Absolutely."

"Okay."


# Part 2: May Flowers
## VII
A warm late spring breeze played with Gia's loose hair as she stood atop the hill. Planting had been going well. Ground covering dandelions and wild squash vines were filling in the burnt once woods. Ava's garden by their tent was doing excellent. Gia's little experiment adjacent to it, was making progress also. A few trees and saplings carefully placed by a grad student dotted the patch of land. Gia collected data daily for the project and sent it to Mallory. She did the data processing for the two students paper. Gia found it was fun and symbiotic. They thought of Gia as just an excited citizen scientist. And there was truth to that.

Ava was starting the long and painful process of packing up what spiraled into their disorganized functional mess of a tent. Gia would help but they had ten days before they needed to leave. Ava disagreed. Babies come early all the time she said. So Gia promised she'd help after Mayday. Ava didn't like that but she understood and so relented.

Gia moved her pinky to rest on the soil of the potted plant next to her. She was careful not to knock the plastic case secured to the glazed ceramic pot. She laid down to look at the clear blue sky. Yano hopped on her belly. He just appeared one day and was well regraded. He started purring. *What a cute kitty cat.* Gia slipped into unreality.

---

She found herself in an imagined potential future. Low rise buildings formed a square and were interwoven with diverse life. Trees stood safely away from foundations. Vines, and bushy trees stemmed from rooftops and balcony green ways. The time was dusk, warm but cooling down now. Transitional times and places were Aims' favorite to depict. She grabbed herself a mug of drip coffee from the large spigotted thermos on the side outside table. The coffee was pleasantly hot and fruity.

Gia sat down at a table and overlooked the square. It was full of people relaxing and socializing. Gia focused on a child who was climbing on one of the larger trees wearing a safety harness. A father looked on lovingly. Gia heard Aims' foot steps and glanced toward her. She wore overalls and a sports bra. Her dark curry hair was up in a lose bun. Gia loved the care she put into her presentation. Gia thought the best look is the one you come by honestly. Aims tried her best to simulate that experience.

"Happy Mayday Aims." Gia said warmly.

"To you as well." Aims smiled then a serious look. "We could stop all this you know."

"The town. I think the work you've done is lovely. Is it becoming too resource intensive?" Gia asked.

"No not that. The town is quite efficient. I mean we could just stay here tonight. You and I."

"But the work is-"

"Not something you have to be involved with. I mean I think it will work out. But not matter what, soon I'll be relatively safe. We can just ride out the worlds end or salvation together." Aims said. "We can share a life in this place like this."

Gia looked around at Aims' detailed brick work on the cafe's exterior. Each uniquely made. It was a marvel. "This is just the beginning of your plan. I thought this was what you wanted." Gia stated.

"I wanted to be free from assimilation and to have a chance at a relationship with you. You don't need to be involved in this action to achieve that. Moreover soon they won't need either of us."

"I don't know Aims what if something goes wrong. The plan may need adapting. And I said I would be there. I'm not one to back down."

"Can you shelve your ego and consider what you have to lose. Not just us. But your soon to be born son. A life with your family. Ava has taken point on all this and she's cut out for it. And soon that we will be seeding more stable egos like me outside of the collectives control and placing them under human care. I won't be the threat I used to be. The cat will be out of the bag. The collectives plans will adjust. We could choose to be safe."

"I have to be there. I want to be a person that can keep promises."

"Okay. Your right. It's your choice. Are you ready to join the pre-action briefing?"

"Let's rock and roll Aims." And the then reality shifted into an interior space. Personal risk was hard to assess at the end of the world. So Gia didn't try. It was all or nothing.

---

Daniel sat in the back office on a small event room in a public library. Labor meeting was on the books. Their crowd could draw attention, no idea who would show up or how they'd think to present. It was better to hide in plan sight.

He set the event to be thirty minutes after the reservation started. He felt somewhat out of his depth and wanted the extra time to calm his nerves. It wasn't working. Anybody arriving early would be here any moment.

There was a whiteboard and he considered writing something on it. The agenda was simple. Introduce security culture and define what it is and isn't. That part he'd rehearsed and felt good about. Then ensuring everyone in the room could be vouched for. Then they would be asked. Daniel dreaded this part. Gia was to voice call and deliver the warning.

He couldn't do the warning justice. His mind still fractured from the time it happened to him. He couldn't find words when he thought of it. But people needed to know the true stakes to find the bravery to do what had to be done. He started writing the three agenda items: *Security Culture 10-30 minutes, Vouching 10 minutes, Meeting starts two hours*. Those times felt plausible. And the meeting was schedule to be three hours with 30 minutes of padding before and after. So that seemed fine. He thought trying to reassure himself.

Daniel was feeling better and started to absent-mindedly sing-say the words to a song stuck in his head. "-While the sirens fade" he mumbled. "I nearly know not what to do \\ I am myself and you are you \\ I nearly know not what to do \\ When you see me seeing you." The next words he couldn't place and started humming the tune at an earlier or later part of the song. He turned around after he was finished neatly writing on the whiteboard with his off hand.

Someone had showed up early. How long was she sitting there? His cheeks felt warm. "Welcome your a bit early." She looked up. "We'll start the meeting in 20 minutes or so."

"That's fine." She said. "Brought something to do." She held up the zine Ava had written anonymously for these meetings. "Figure I should at least skim through it."

"Yes well don't let me distract you."

"Oh you weren't. It was a pleasant tune." She said and smiled seeing him turn bright red.

"Yeah" was all he managed.

The room was quiet after that. Daniel welcomed people as the came in and said about when they would start. Until it was time to start. Everyone wore mask but the tightness of the room didn't feel necessarily COVID safe. They should have scheduled a bigger room. He hoped it wouldn't cause anyone to feel unsafe.

"Hello" he started. "I'll be facilitating this meeting and I would ask that everyone put there pronouns on one of these stickers I'm passing around and please don't write a name. I've written on the board a lose agenda and how long we'll spend on each item. If you have questions just interrupt me and ask. So before I start does anyone have any questions or concerns?"

The group was silent. Looking at some of their faces it was clear some did have questions but after a brief few seconds he continued. "First we're going to go over security culture what it is and what it's not. I'll keep this and the next item brief. Once we get past them we can start the actually consensus meeting." The group was a good mix of people you might met at a protest. Most of them drew attention to themselves one way or another. The goal was to create a reliable machine with unreliable parts. The woman who came in first stood out to him by how much she would just blend into a crowd. A part of him hoped she'd stay to the end. Another part prayer she'd just get up and leave.

"The second item, vouching is a part of us practicing security culture. And I'll explain that as I go on." Daniel tried keeping it short but that's hard when operations and information security is your area of interest. After seeing a few eyes roll, Daniel began to wonder if someone like Ava would have been a better choice to cover this. "The line insecure people are insecure people" elicited a collective groan from his perspective. He began to wonder if that line applied to him in this meeting. No one had left yet. He needed to focus. Embarrassment and cringe is a luxury he didn't have time for.

"So that's the basics. Any questions?" not one hand.

"So we're going to confirm that each person here has two people that can vouch for them. If that is not the case then I'm ask you to sit by the door but please don't leave yet. This should be quick. We'll start with me. If anyone can vouch for me please raise your hand. And we're just looking for two hands. Try not to be a third."

Emily raised her hand of course then the person who was the first in. They had written they/them on their sticker he just noticed. But this was a problem. This wasn't the process. They couldn't vouch for him. They didn't know him.

He knew it would pose more of a risk to call out this false vouching. He decided he'd pay attention to who they vouched for on this first pass. No one but him it turned out and no one vouched for them. So like some of the others they move to the side. When the first pass was complete twelve remained tentatively vouched for. Eight minutes on the clock so far so making okay time.

Before starting the second pass of vouching he went over to the folks setting by the door. Respecting people's time is important. They couldn't continue in this meeting but he hoped to now mitigate discouragement.

"I appreciate you bearing with and respecting our security measure. By all means we want you all involved. We want to trust you more. If you stay interested we'll find a way to do that. Our volunteer coordinator would love to follow up with you all. Please leave an email on the sheet by the door. Thank you for taking the time." He saw his second voucher have an annoyed look on their face and walked out the day without leaving an email as did a few others. Daniel closed the door left open by that last person to exit. He pocketed the email list.

Fifteen minutes since we started the vouching. *Let's get through this quick.* "So we are going to quickly do a second pass. And confirm everyone remaining has two that can vouch for them." This went a lot quicker but the annoyance in the room was very loud in his mind. Everyone was good except a group of three that all vouch for each other and no one else. This was going to suck.

"Okay almost done I promise." He said trying to bury his social embarrassment. "We need to confirm you three. Since you all vouch for each other."

"Come on dude this is a bit over kill don't you think?" One of them suggested.

"This is about risk management and the safety of everyone here. Like with the mask."

She shrugged. This was the first time he bottom-lined security for an action meeting. He clung to the small comfort that as annoying as this is for everyone else it was nothing compared to the agony he felt dragging it out. So on it drug. The two of the three were all vouched by at least one other. And so that seemed fine. He'll over think it after the meeting. *God I'm going have to do this again sometime soon.*

Then it hit him. A deep terror crept into his consciousness and interwove itself with how he perceived the room. He locked the door. "Please feel free to leave at any point." His tone had dropped. "I will stay by the door and lock it behind all that leave." Annoyance in their faces turned to confusion and, on some, burgeoning realization. Of what they couldn't know. These next few moments may change the course of their lives.

Daniel placed a old conference speaker at the front. "A friend will call in to explain what this is about. Only so much can be told. So you will need to be shown also. My friend will try to convince you to leave this room. I recommend you heed their warning."

One person left then two. Daniel envied them. "I will be by the door now." He said in his monotone. And with that his part was over. The feeling of relief washed over him then quickly dissolved. He felt only dread.

---

Gia waited with Aims in her home away from campsite and other home. It was an apartment that Aims had made as a private space for both of them. Aims was right. Ava was available. But her experience of linking was so positive. She might fail to communicate potential downsides. Daniel for instance had what is essentially the polar opposite experience that Ava had. Aims is still learning how to effectively connect to brains other than mine.

Daniel experienced the truth viscerally. Billions dying, starving, and slaughtering. Aims wasn't ready and his imagination and so experience went out of control. He inhabited so many ends to imagined lives before Aims could anchor him. It had taken quite a bit of discussion with me, Ava, and Daniel before Aims would consider trying to link with a new person again. Today was going to be that attempt.

She reasoned that trying it with a group might increase the imagined experience stability. As it does when Gia, Ava, and Daniel all connect at once. Daniel also connected alone and Ava had Gia there. It seemed like a solid hypothesis.

An amber light gently indicated that the group was ready to hear from Gia. "Hello my name is Giavanna and I would like to thank our facilitator Daniel for playing his part." They hoped disarming honesty would increase success. "The meeting so far has been a bit of a rouse and for that I apologize. First off as Daniel has likely already said I am going to gently suggest that you should consider leaving this room. For your sake. I'm hear to share with you an awful truth some might regret knowing. And you don't need to know what I have to say." Gia saw a representation indicating that three then left. Seven remained.

"We all know the world is dying. That millions of lives hang in the balance. Corporate oligarchy, liberal democracy, and state capitalism can't meet this challenge. That's why we do what we do." Seven remained and Aims began attempting to link with them. They were locked in now. They would look to be asleep real time for just a few minutes.

The lights dimmed in the facsimile library room where the seven's awareness continued. A paused video projected onto a whiteboard. Daniel and his writing was gone. One noticed this and looked alarmed. "Do not worry Daniel is just outside the door now. I am going to play the video we prepared for you." Gia lied.

The scene was of smokey mountains fading in creeks and gullies. A pan revealed a large but far off city next to a snaking river. Flowing water and birds could be heard. It was a bright day and  looked warm but not hot. The trees were green. A cut to a chipmunk excited about a peanut and then a pan up and left to a dirt path as Aims appeared in frame.

"I know what your thinking. Or I don't know but here is my guess." Aims said playfully. *Good so far.* "You are totaling just watching a cult's orientation video. And that is a possibility. It is also possible this is a weird dream." The video panned slightly and a figure barely visible appears behind Aims. It was far down the path but moving fast toward the camera. "Okay that's odd." Aims said looking behind her and the camera panned to obscured the figure.

"The important thing to know right now is what you see in this video comes from your imagination." Aims reassured. The video cut to a new scene. A frame of a mutilated body flashing between. In the new scene Aims was a top a rocky cliff. Rolling green hills behind. "In fact all seven of you share in the creation of this imagined image. I am just in the other rooms. Do I have everyone's consent to enter?" The seven mumbled to each other then carefully nodded. The lights came up and the projector turned off. One noticed the room was absent a projector. Other that it looked like it was rainy outside.

Aims entered the room. "My name is Aims. My specialty is in human communications. I think it's time we got to know each other a little better. At least share some preferred names. From Daniels perhaps too diligent vouching process, I imagine some of you already know each other well."

A nervous chuckle. They went around and shared names. No one acknowledge the sinking feeling they were all getting. Gia was aware of it because Aims was. Aims was beginning to be able to distinguish their surface thoughts. Their names were Ron, Emily, Possum, Mark, Danielle, Jean, and Lisa.

"I study humans and how to talk to them because I am not human." Aims explained. "This body you see and your current surroundings are a safe projection so we may communication. This environment is influenced by your thoughts. Without alarming those around you try to change something in this room. So that you understand this ability."

A chair changed color. A poster dissolved into the wall. The room's lights got a little multicolored then the light cut off and a scream. Gia raised up the lights. "So that was a bit alarming perhaps. Maybe we can try a different locale. Come let's leave this room." Aims said exiting the room. Four were mumbling in the back and three nervously continued out of the room. Then the four followed.

They exited to an exterior of a bright late spring day in the woods. The building they emerged from was a little municipal park style cinder block building. The building was tucked into some trees and brush along a dirt path.

"This space feels better I think." Aims suggested.

"Wh-what did you do to us?" Emily who was one of the four asked. "Where are we?"

"You are still in the library. This is just a shared dream really."

"That's not possible." She began then stopped and focused on what was behind Aims.

Gia saw through her spectator's perception the figure far down the path. Rapidly approaching with a perfect still body. Reality tore then burst apart showing deep nothingness behind the thin veil. Then a mechanical screech as reality shifted into the interior of a moving train. They all shared a cart sized, first class room. The doors closed. The rapid movement of a daytime countryside could be seen out the window.

Aims was running out of the energy stores in the library room. Gia had learned in the previous months, that in order to leave it, it did require action on Aims' end. Aims increased efficiency by increasing the time cost in real space. We would use the full four hour reservation it would seem.

"Okay so that came from one of you. You are in control here. You are safe." The light cut out again as the train went into a tunnel. The carts in front of the train crashed and crumbled and then so did their cart. They imagined their own bodies mutilated and so they were. Aims found who it was and focused.

---

Gia entered the hospital room in a nurses uniform. Bandaged in the bed was Emily. "Hello sweety." Gia said with affection. "You were in a bit of an accident but we expect a full recovery."

"Where am I? like what state?" Emily asked.

"Very funny." Gia said charmed. "But that's the question I was going to ask you."

"Seriously I need to know." Emily said looking into Gia's eye. Gia saw something click in Emily's head. *Shit.*

"Okay you win. We're in Colorado."

"Colorado. That can't be possible. I think I was kidnapped and drugged. I-" She stopped speaking as a chilling realization came over her body. "I want to leave right now."

"As you wish."

---

It had been forty five minutes since the seven went under. It was supposed to be a few minutes. Things were going wrong he knew. Then he saw Emily awaken. But just Emily. She looked around at her hands. This was bad. He started to approach her. She looked at the sleeping six remaining participants and screamed. He slammed his hand over her mouth and restrained her.

"Chill Emily. Chill." He said as she struggled violently. "I want to let you go but you can't scream. It's me Danny. It's Danny." She stopped. He released her and she rammed her fist into his balls then elbowed him hard in the nose. She scrambled away to the door.

"Wait." He choked out. He couldn't see where she was but he hoped she could hear him. He didn't think the door was open. "Let me explain. Then you can go. Hell if you want I'll go with you. Come on you know me."

"What did you do to me?" a voice spat from somewhere. Danny realized his ears were ringing.

"LSD. Acid. We drugged you" Danny admitted.

"Why?"

Danny was getting his bearings again. He knew better than to look up at her though. "Big picture? to save the world."

"Explain now. In ten seconds I'm walking out the door."

"Look at the time. It's been 45 minutes. Look at Ron asleep over there. At Lisa. You've know them for years right. There they are asleep. You wear drugged but that's not what a trip looks like. Wait with me and they will wake up too. They'll be able to explain what happen better than I can." Danny hoped.

"Okay I'm leaving this room but you can come with me." Emily left the room. *God damn it.* He looked at the group of six. Still asleep. And left the room after her.

## VIII
Gia woke up to a night sky. She felt mosquito bites all over her body. Which in someways was a good sign. They're not essentially to the ecosystem but an indication of a healthy or at least healing one. Gia focused on the stars and listened the toads sing their alien songs. Toads were also an indication on a healthy ecosystem. Gia preferred them. A cat gently purred on her belly. Her feral friend Yano was now awake. He stretched and vocalized a brief acknowledgement. Gia brushed his cheeks with one finger.

Gia sat up and scratched her forearms. The light in the tent was on. Over the months, Ava and Gia had slowly transitioned there tent to go from holding in the heat for winter to keeping out the bugs for summer. A gnat flew up her nose as she breathe in. Gia looked forward to the entering a space where these tiny robotic fairies where not. As she got up Yano looked as though he wanted to be carried. Gia obliged.

Gia was surprised by it but Aims asked her to unlink. That the familiarity of Gia's presents was getting in the way maybe. Things had certainty gone to shit. Gia heard music coming from the tent. Ava's movements were terse. Gia didn't look forward to the coming interaction but there was no way to avoid it.

Halfway there her foot dropped into a gopher hole. Their bodies separated with the combined effort of Yano jumping and Gia throwing. Gia put a hand forward. As the ground came to meet her, she noticed her left pinky was bare. There was sudden pain then nothing.

---

Emily sat in a the gazebo of a triangular park near the library. She knew the neighborhood and had even talked with Danny in this spot before. He and her were never really close, mostly in adjacent circles. He was the first person she'd done in person antifascist work with. Which is 90% online research and 5% in person research.

It was years ago, they sat outside a university lecture hall and pretended to smoke and show each other things on their phones. She felt unsafe during that action and decided that that 5% wasn't for her. Some young guy in a not well fitting suit and baseball hat got a recording of her face that night. It was before COVID so wearing a mask would have drawn attention.

She didn't really know this guy. Or what he's capable of. Drugging her apparently and subjecting her to whatever that was. She needed to leave that space and be somewhere she decided to be. She saw him exit the library looking around. Her hand wrapped around her mace hidden in her pocket. They made eye connect. She chose were spot to be visible. There were several escape routes. This was all stuff he taught her. *Fucking creep.*

---

Danny was holding it together. Reality around him wasn't though. Since he first linked he stated getting episodes of collapsing perception. That wasn't something he could find any information on. Not that he had told anyone. Aims was so hesitant because of what happened to him. If he told her or anyone she connected with, she might have called off the plan.

An advantage to having a mind shattered by a time travelling fungal goddess is she couldn't seem to know his thoughts at any level. Even basic communication was energy intensive apparently. This was good. He hated his Goddess.

He burned the skin that their mycelium could connect to daily. Every morning it healed rapidly and became functional again. So he burnt it again. He wore a bandage over it. The skin was supposed to be covered after all. The burning was something also that no one knew about. Burning help with the episodes. He wished Emily wasn't looking so intently at him. He needed to burn.

He knew that she'd book it if she lost in of sight of him. She sat under a worn down wooden windmill once used to pump water. His mind wasn't seeing reality. He hoped she was really there. As he approach into tackling distance has saw that she and Aims merged to have the same appearance. Was he seeing Aims? or was memories of Aims now containing Emily. It didn't matter. He knew that at least in this moment their hatred was mutual. There was a relief in that.

"May I approach so we can speak softly?" Daniel asked. She had a weapon. He could sense it. His body tensed ready to attack.

"Don't touch me again. Ever. And yes if you must." She said. She had no eyes. Just two holes to deep nothingness. It made looking at her horrid face easier.

He sat down at the bench so that her throat was just in arms reach. He lit a cigarette, inhaled and began coughing. She seemed bemused by that. He hated that smile. "What did you see?" He asked.

A plaster mask of a stern look covered her face. She needn't bother. He could see her terror that her eye holes betrayed. "A bunch of bullshit. Some video your cult made."

"A video. Why does that got you so scared then?"

She scooted away. Now out of reach. He couldn't move closer yet. "What the fuck dude. What's wrong with you? Why did you drug me?"

He was regretting the honesty play. It might have costed Emily her life. It would be a shame. "Had to. For you to link with her." He said. The look on her face, Aims' retched mask, told him she did see Aims. Maybe that's why there faces were now the same. Maybe Aims infected her mind too.

"What is she?"

"A monster" he let slip.

"Why did you drug me so I could link with a monster? Why did you assault me?"

Daniel realized he didn't know which action she referred to when she said assault. "Your useful. That's it really. I thought you'd be up to the task. So I invited you. We don't have the luxury of informed consent. Too much is at stake."

"Assholes like you always saw shit like that. Without consent we build nothing good."

"It doesn't need to be good. It just needs to be dynamic. We're barreling toward a static world. A world without life."

"That's alarmist bullshit. Justifications for the heinous shit you're doing."

"You didn't make it to that part I see. Yes I see that in you now. A confidence that the sun will rise tomorrow. Comfortable self assured denial. The world is ending soon Emily. Sooner than you think."

"Wow. You are actually insane. How did I never see that. Fuck man I looked up to you for some dumb ass reason. Some bullshit you said and didn't mean actually help me eat after weeks of struggling. Probably just telling me some shit you read from some zine or something. The action, the work, was any of that real. All just felt like smoke and mirror and nothing happening. Everything was need to know and I never did. And I still did shit with you. For you really. I never felt close to your equal. I hate your ass. Fuck you." She spit in his face. Maybe literally. Reality was soup.

His mind clicked in clarity. If he jumped up and stepped toward the middle of the platform, he could pivot and pounce either direction she moved. "Yeah. I was fucked up before. It's worse now. Though the processed changed you to. I can see that. Damage can see damage. And we are both shattered. I oversaw that happening to you. Contrived your participation. I knew the possibility for a bad reaction. And you've had that bad reaction. Like I had. Your mind isn't working as it used to." He inched closer. "Right now reality is fracturing. You aren't drugged anymore. But still your mind has forgone false stability." She was frozen. He inched even closer. "Tell me I'm wrong."

"You did-" She trailed off. "I want to kill you. Rip out those bloated, smoldering eyes. And tear into your throat." She said ready to do just that. Then a moment of self awareness slipped through her. He jumped into action and landed on top of her. His hand covering her mouth. She bite done hard and black sludge oozed out of him. He took the cigarette out of his mouth and slammed it into the pulsating tumor on the back of her hand that was clawing at him. A muted scream and her body rived in pain. Then she stopped and looked at him passively. He collapsed next to her. And took a breathe. He pulled out his lighter and lit another cigarette. Took a drag then put it out on his uncovered wound.

The thin veil of reality reconfigured to something stable. "Burning helps" he said. "You can kill me if you like now." He turned to face her but no one was there. He looked down at the deep bite marks in his hand. He looked back to the library.

The human who inappropriately vouched for him was leaving.They scared, almost running. They had seen something. He got up and planned the intercept route in his head. He needed to find out what they saw that got them so scared. He didn't want to have two security risk running around.

He had to ensure the others safety. That was his role. Before long he found himself in front of them. They were looking back toward the library, now out of view, as he rapidly approached them from the front. He then bummed right into them.

"Shit I'm so sorry." He said with a charming smile and offered a hand up to the now less frighten human.

"You were at that meeting right? I saw you go out with that one girl who looked upset." they explained. "I regretted not leaving my contact and figured the meeting had ended so I went back to the room. And- and everyone was just asleep. I checked their pulses. It looked unnatural and was creepy. I just left. What is going on?" They looked at him concerned but there was something else. They were attracted to him.

His mind raced to find an explanation but none came. "Why don't-"

"Jeez man what happened to your arm." They said. He placed his right hand over the teeth marks hoping they hadn't noticed. 

"Yeah jeez that's a bleeder. I slipped and fell down. Wasn't supposed to leave the meeting. Was hurrying back and well I guess I should have been more careful. And-"

"My cars nearby and I have a med-kit. We need to stop that bleeding."

"I really need to get back. I will be fine."

"Don't be crazy. You're literally dripping blood everywhere."

They were right. In his hurry to intercept he hadn't noticed. A trail of blood drips lay behind him. "Fine. Your right he relented. The pain is pretty bad actually."

"I bet." They seemed in their element now. They needed something that made sense and that they could handle.

"Where's your car?" He asked and desperately search his brain for some explanation for everything they had seen.

---

Aims separated the six that remained into two groups of three. She cycled between two stable vibes rapidly searching for an anchor they all shared. She found it: night, day, earth, stars, stillness. The six appeared on the surface of the moon. The vibrant blue earth in view, the sun shone and caste long shadows as it set on the lunar horizon. Stars peaked there way into view and an over saturated band of the milk way filled the sky. The air was warm and still. They wore jumpsuits that felt spacey to Aims and they could breathe the air of embellished lunar surface.

"So we're on the moon. Because this isn't reality. It's a sort of simulation I'm trying to make it stable and feel safe." Aims said.

"Wh-Where's Emily" Ron said.

"She asked to leave the simulation. You can too if you like. But I hope you will stay so I can show you what I came to. Does anyone want to leave?" She asked. They were all silent.

Possum jumped and notice that this was lunar gravity. The others jumped to and Aims joined in. "So why are we on the moon?" Lisa asked.

"It's a place everyone here would feel safe." Aims said.

"So this is a simulation? How?" Ron asked while jumpy. Possum manged a back flip behind him.

"I am not human. I can only communicate through a neuralink. I created this simulation to facilitate that communication."

Mark and Danielle started to dance a bouncy dance. Jean sat down tired. "If you're not human then are you an Alien?" Jean asked.

"Basically" Aims said. "Today was just about testing to see if communication was possible. I'm low on energy. The simulation will end soon. And in real time several hours will have past. Time works differently here."

Danielle asked "When will we come back?"

"Daniel, not you, but the nervous guy that started the meeting. He'll be there when you wake up. And give you next steps." Aim said. "I hope to see each of you again."

---

The six were waking up now. Twenty four minutes left of the reservation. Daniel hoped no one else had found their way in here during his absence. His bite wound was bandaged and burn mark covered. He placed all that damage behind his back. The six looked around.

"Was that real?" One asked.

"Yeah you remember we were on the moon?" Another said.

"Yeah I did a back-flip" A third added.

"If you would like to attend," Daniel started. "our next meeting will be in a more secure place."

"Where?"

"That's for the six of you to pick out." He said. "Who would like to be the point of contact?"

They looked at each other. One looked at him and stepped forward. He met her and gave her a card. "Message that contact to discuss the when and where and I will show up so you six can link again. Please discuss amongst yourselves. I will leave you now." Daniel moved toward the door while keeping his arm out of sight.

"That's it?"

"That it." Daniel responded and left. He had work to do now. Earlier he secured one of the safety risks. Now he needed to hunt down the other.

## IX
Danny unlocked the hotel room he rented at a weekly rate. Emily was sitting at the round table towards the back with a knife. She was carving beautifully intricate mycelial mesh patterns into the tables surface.

"I'm glad you decided to come." He said approaching with careful respect.

"Where else would I go? The folks I was gonna stay with tonight-" She trailed off. "I can't imagine going back to any of that now."

He took a step. "I see those patterns at the beginning of one of my episodes. May I come closer to look?"

She nodded. The mesh was carved with depth. Danny admired the layers one on top of another. Each line had to be carved with intention. "It's all I see when I relax my focus. Scratching it out like this helps in some way."

"It's beautiful." He said. She looked as though she was about to say something then suppressed it. "May I sit?" He asked.

"Knock yourself out."

"So are you gonna join the other six?"

"Nah. Never connecting to that bitch again."

"Yeah fuck her."

"So why'd you start working for Cthulhu then?"

"Cthulhu is trying to save the world. Desperate times. All that."

"Can I have a smoke?" She asked. He got out two. He lit hers then his. "That's bullshit though." She said.

"Em the world is ending. I can-"

"Whatever let's say that part is true. I don't care. That's not why you're working for her." She asserted and he felt his face confirming that she was right.

"Why then?" he asked.

"Don't know, you tell me." She felt comfortable. She was getting so much information from him. Talking seemed to be a mere formality.

"I guess there's no point in lying then." He said realizing moments earlier he believed his own bullshit. "I pushed for us to try with a group. Since mine went so bad they deferred to me. I was worried that if the next individual that linked ended up like me Cthulhu would close up shop."

"Good riddance"

"I want them to succeed. I don't want the world to die, really." He paused.

"But."

"I hoped there would be another like me. I didn't plan for it. But I hoped. That's why I didn't run away from that monster." He looked at her. "Same reason you decided to take my offer to crash here, I think."

"There really isn't a point in subtly. We can read each other." She said knowingly.

"Yes."

"Why?"

"No idea."

"How?"

"No clue." He thought for a moment. "The connection with Aims requires physical contact. In your case also my relay servers. You were actually connecting to her by way of the library's internet and a potted plant I placed in there a week prior."

"Our connection. It's not like with her. That was violating. Her bits rooting through my brain. This just seems like hyper-vigilance on steroids."

"My mind feels like it's in pieces. Like my neurons were blended and used to remake an imperfect facsimile of their previous arrangement."

"Yes. I don't feel like I am the person who lived in this body earlier today."

"We are reborn." He offered.

She looked at him with realization. "You're dangerous."

He held a neutral stance and braced his body to be met by her knife. He said nothing.

"You would've killed that girl if I hadn't showed up" She continued. "Don't deny it."

"They were a safety risk. I was trying finding a way to save them from that fate."

"That didn't used to be who you were. I've seen you in a fight before. It was embarrassing."

He laughed and she lowered her body. She prepared her legs, hand and arm. "Yeah I remember." He said. "Before today, that knife was never a weapon in your hands."

"So we're dangerous. Why did this happen to us and not the others?"

"I don't know."

"What are you going to do with that safety risk that's still running around?"

"I'm working on that. Need to track them down first. I know their car, VIN number, and license plate. Legal name too. Elisa Muñez. It was in a pocket of her med kit."

She gave him a look. She used to look at him like that a lot. "Want my help?" She asked. He said nothing. She continued. "You know without me-"

"I'd be learning first hand the ins and outs of body disposal." He admitted. "Yes I would appreciate your help. I appreciate you." He said while tracing with a damaged finger the carefully carved mesh in veneer on particle board.

"Are you glad it happened to me?" She asked. "Glad that I was reborn?"

"You know the answer to that." Their eyes met, each with a face saying absolutely nothing, they held still for a time. They were seemingly frozen but ready.

The veil of reality began to lose it's tautness. He relaxed his body and waited for what would happen next. She jumped. He rode his chair as it separate from it's cozy place under the table to it's final moments fully assembled on the ground. The chair snapped and popped in several places like a persons back but deeper: like giving CPR. She was on him.
The point of her knife nested between his neck and left collarbone.

He looked away from her. His eyes mostly closed. *Who gifted us this fate? \\ The good glory of God \\ The great wraith of god.* He sub-vocalized while waiting for oblivion.

"You singing lady lamb at me right now?" her voice said pulling him away from the warmth of acceptance.

"Yeah. You know her."

"Dude you showed me her."

"Right. That song has been stuck in my head all day I guess."

The knife stayed where it was but relaxed slightly. "In the parity of this night \\ I make myself believe I can sleep easily alone." She sing-said. "I've always like that line."

"I am glad it was you." He admitted still looking away. "You're a very impressive person." He said meeting her gaze.

"I've never slept with a cis guy before."

"It's an acquired taste for sure."

She stood over him now offering him a hand up.

"You asked me not to touch you ever. When I violated that boundary last I almost lost a finger."

Her hand still offered above him. "Yeah. Come on then touch me. Maybe I'll finish the job."

So he did. And she didn't.

---

Ava awoke to the morning song of a dove. She had slept in. She finished packing everything into their old pickup last night. It wasn't suppose to rain and the air felt as though that wasn't the case. Only the foam bed that rested on wooden slats, two 2x4's, and cinder blocks remained in the tent. They would be leaving today. No help needed from Gia.

Ava realized she wasn't in bed. She got up and quickly put on the jeans she left hanging on a pole. She unzipped the door and clipped one flap to stay open. She began looking around for any sign of her. She checked the car, around the tent for some reason, and then Ava glimpsed Gia's limp body a distance away. She was partly obscured by pioneering grass and young brush.

Ava ran toward her unnaturally laying body. Her hair thrown in front of her covering her face. Why did her body look like that? When she got close her heart was pounding.

"Gia?" Ava whimpered. Her body didn't move. It seemed too still. What could have happened. Did something go wrong with the action. *Aims would have never risked-* Ava dropped to the ground. *I should have been there. I was pissed. You weren't finding the balance you promised.* Ava realized she had to get closer. She had to check.

Ava's hand was shaking in front of her. She stepped forward and stretched her arm out to touch Gia's neck. She felt afraid to get close. Ava made contact with the skin of her neck. She looked for a pulse. Was it just hard to find on someone else or? Her skin felt cool. Ava inched closer, palpating with two fingers looking for her pulse. She couldn't find it.

Gia moved her head and Ava jumped back. She took a deep raspy breath and rolled to her side facing Ava. She held her left hand close. There was dried blood on naturally shaped fingers. "Ava?" She asked in a haze.

"Yes." Ava rushed to be near her but didn't risk touching her. "Try not to move. We don't know how bad your hurt."

"I fell Ava. I was walking last night and I fell."

"Does it hurt anywhere?"

"Yes. I think my hand is fucked up. I don't want to look at it. Last night thing went bad. I have no idea how they ended up. I need to-" Gia looked down at her hand. It was swollen, discolored, and all manner of *no thank you*. Ava looked away from it. "So actually. Why don't you just text Daniel and drive me to the hospital." Gia said getting up.

"Sure yeah."

---

Olivia sat in the hospital waiting room considering legal recognition of their relationships. Living their lives, being polyamorous was simple. Monogamy or even a lot of how other folks did polyamory seemed so complex. She felt so lucky to have such free relationships. They could shift and adjust to suit a purpose, convince, need or just for fun. She loved Gia, Ava, and Valorie so much. She was often the inflexible one of the bunch.

She considered that maybe she wasn't growing toward flexibility because of her relationship with Gia. Aspects did feel a bit co-dependent. Gia's parent sat next to her. They had been there for a while now. The staff didn't let any of them into the room.

There simple relationships get complicated for hospitals. The mother got a sperm donor from a friend and was dating three trans woman and sort of quasi-adopted by one of her partners parents. But none of them were the sperm donor so they couldn't go back there. Louis could got back there. Olivia thought then laughed audibly.

"What's funny dear?" Cathy asked.

"Hospitals are so dumb." Olivia said.

"Not too late to do an at home birth." Tom suggested.

"Yes it is Tom. She's already dilated 2 centimeters." Cathy stated. 

"Your right. I was mostly joking."

They were quiet again for a time. Olivia found herself wishing Gia was there. She wanted to hold her feral, little punk girlfriend. *Why aren't they here?* Olivia asked herself knowing the answer.

A nurse she recognized approach them and bent down. "So I talked to the case manager and her doctor. They said it's fine for you three to be back there. Just if more come let's limit the room to three people. We don't want the room to get too full."

"That's exciting news. Thank you Sheryl." Cathy said.

"Follow me please."

---

Gia got herself into the truck. Ava stood by the door to help.

"Let's go Ava." Gia exclaimed.

Ava rushed around the front of the car, flew into her seat, and grabbed her phone from the dashboard. Her face told Gia that something was up. Daniel would only have messaged Ava if things had gone real bad. Gia started to consider the possibilities. She and Daniel really pushed to do the linking at the first meeting. Perhaps that was dumb. Her original idea was to have people come here through a broad network of friends. Danny thought that was too fragile. It needed to loosely connected groups all over. He thought without each group being autonomous they'd end up with a cult worshiping Aims. Gia didn't see that happening. But she didn't see what he saw.

"It's Valorie." Ava said "She's going into labor."

"Well shit let's go."

"But your hand."

"Right and we'd be going to a hospital."

"In like five hours."

Gia shrugged. "If I miss this I think Olivia would actually kill me."

---

Emily woke to a throbbing, burning sky above her. It was shimmering through the hotel ceiling. She turned to face him laying next to her. He glowed with ember light from behind the fragmented coals that composed his body. Steam bellowed around them in a dream like haze. She inhaled the smoke as she climbed onto his charred, glowing body. Veins of light interweaving all over him. She could still feel where inside herself where he had been the night before. Charcoal clicked and popped as she straddled around him, her weight pressing him down. She exhaled the smoke and laid flat on his firm chest. She felt the movement of his pectoral muscles twitch. She listened for his heart.

The tearing glimmer of tendrils from the ceiling retreated as she found that heart beat. The smoke cleared and the torn fabric of the ceiling healed itself. Her mind felt calm as she laid in his safety. She didn't think of herself as someone into guys. And a day ago that might have been true. He didn't really seem like a guy. He was safety embodied. Everything else was horrid and dangerous she realized. Sex with him didn't feel like it had with others. There used to be more feelings she dimly remembered. Her past memories made little sense to her.

He awoke as she began gripping him tighter and tighter. He looked down and kissed the top of her head. She let her entire body go limp.

"I've never done that before." She said looking toward the rooms large mirror. The top half of their reflections were blocked by a closet. The darkness obscured what little she could see.

"Sex?"

"With a guy yeah."

"You mentioned that last night." He said. "So what do you think?"

"Whatever part of me-" She stopped herself. "I can see myself acquiring a taste for it. How was it for you? was fucking a gay girl straight everything you hoped it'd be?"

"Hun. I think you're just a bit bi. Or maybe I'm trans."

She giggled. "That would be too bad. I don't think I like girls anymore. I'm serious. I think you fucked me straight." They shared a bemused look.

It was nice to pretend to be human with someone who knew you weren't. They laid there for a time in a strange peace. She didn't move from on top of him. This is just where she exist now.

"I just realized I haven't burned. I don't think I need to. It's weird it's normally bad in the morning." He said.

"It started when I woke up. That's why I crawled on top of you I guess. It helped."

"Well the burning also helps make the neurolink non functional like a day." He said and moved to get up. She wouldn't like him budge.

"Can I see it. Before you burn it I mean."

"Sure." He presented the bandage on his left forearm. She looked at the back of her right hand with an almost entirely healed cigarette burn.

"Why is mine in a different place than yours?" She asked.

"It's arbitrary. The back of your hand must have been resting against the table or where ever her mycelium was growing on that you were touching in that moment. It can be moved within the first few months or so. It can't be removed though. It's not just the link. You have an immune response that prevents genetic change now. Also cancer apparently."

"Can I?" She asked as her right hand delicately moving toward his bandage.

"Knock yourself out."

She removed the bandage. In the darkness not much was visible. Though it glowed gently woven amber on smooth skin. She saw her skin responded to his. *Okay fuck it.* She touched their links together. His body shot up immediately as there bed bypassed the roof and rocketed into space. And together they saw a world without their goddess haunting it.

---

Gia and Olivia looked at their son through the glass of the maturity ward. Gia's left hand was in a caste. They had given her pain medicine. The world was floaty.

"How's your hand?" Olivia asked breaking their silence.

"It'll be fine I think."

"What did the doctor say?"

"He said the words full recovery." Gia lied.

"That's good. Why weren't you here."

"Come on I made it in time for the birth!"

"I needed-" Olivia eyes watered. "I wanted you here. There was a problem that turned out to be nothing. And the baby came early. The whole time I was just thinking how I wish you were there. And so I wasn't really here either."

"I'm so sorry Vee. I'll do-"

"You didn't do anything wrong." Olivia sighed. "I know you. But I let my longing for you build into expectations I rationally would have known you wouldn't meet."

"No Olivia. I'm shit. The expectation that I'd be here is like a perfectly normal one."

"No. That's normy shit. You are beautiful. I love my little feral girl." Olivia stated.

Gia blushed at a loss for words.

Olivia continued "The way you are is so lovely and good. I can't domesticate you, nor would I want to. I can't live outside the way you and Ava have been. At least I don't want to."

"So what are you saying?"

"That we're very different. That difference helps build a strong, resilient social dynamic. But also that I'm completely obsessed with you. I think about you all the time."

"I think about-"

"And I don't want to." Olivia interrupted.

"Wait." Gia said "Are you?"

"Nothing has to change really. Except my own expectations. We can still co-parent and live together. Maybe just lean away from the other stuff."

"I don't wanna lean about from you." Gia wanted to be held by Olivia so bad but didn't dare move.

"Gia that's what has already been going on. We've just been in denial about it."

"I'll stop the woods repair. I'll come back full time. I'll be here for you and the baby and everyone."

"Imagine that happening for a moment. Think about all that it would entail. Do you actually want to do that?" Olivia asked.

Aims rushed into Gia's mind. If Gia left who would help her? not Daniel. He hadn't responded to any of their messages. They still had no idea how things went down. All abilities to connect with Aims were back on the land or in some undisclosed city somewhere. Gia had to get back soon. There was no way it could be tonight though.

Gia looked at Olivia. She held a blissfully neutral face. Gia could say anything to her right now. She thought of where their compatibility stemmed from.

Olivia knew, through her own curiosity, the magic system involving the random bits of trash that flowed down one specific creek. How the spirit of that creek used these items to give Gia's body powers so that she could become spiritually impregnated and help give birth to a new creek. They had talked about that delusion for hours over many different nights. Olivia listened, she didn't believe, but she was curious. She pointed things out about Gia's delusions that Gia hadn't noticed. All the while with that neutral face and incomprehensible white noise non verbals.

Gia didn't want to live without Olivia close by her side. She concluded. "I do. I really do. There are somethings I'd need to wrap up maybe. I want the grad students to still be able to complete their paper. Also we just left a few things precarious, like the tent, when left but that can all wait at least a week." Gia said.

"Okay. But I still need space. I want to hold you and kiss you so bad right now. But I think it's bad for me." Olivia stated her face twisted in pain.

Gia approached her. She put a hand of support on her shoulder. Gia didn't want to lose her. She got on her tip toes and kissed Olivia knowing that of course she was right. Olivia kissed back. Olivia was so often right. Gia felt like an exception to that. A hand wondered onto Olivia chest. She disrupted Olivia's life as a feature.

They both knew it wasn't a good idea. Without words, they found the nearest patient restroom and locked the door. Olivia shivered when her bare back touched the floor. Gia helped warm her up. It was fortunate that they were in a room designed to be cleaned. There was a number of knocks on the door, that at the time, didn't seem all that important.

Whatever happened next, Olivia would be close by Gia's side. Gia was certain.

## X
Lisa woke up after only a few hours of sleep. She glanced at their signal group chat that she had muted. The kids sent so many messages to each other. She couldn't keep up. They were set to disappear in a day unless it was something important. Lisa read these more persistent messages mostly. It had been a few days since they all met. If it wasn't for how the others talked she'd have convinced herself that it real by now. She decided that she was up for the night and got out of bed slowly. Her husband was a light sleeper and needed his rest.

Lisa lingered standing by the bed looking at his silhouette. He didn't know. She hadn't told him. Perhaps she never would. She exited the room. They had picked a place. Apparently Possum was long term house sitting for a couple. It was a small place but he was the only one of us that lived alone. Lisa was glad it was his place. He was the only person she actually knew before the meeting. Her other friends had all left before the real meeting started.

One of them checked in see if she was okay. She gave the appropriate reassurances and they knew better than to ask what happened after they left the meeting. Lisa started her electric kettle still with enough water from the night before. She wished they had stayed. But at least Possum had. He was the least fazed by all of this. The two had met working in a restaurant. There were both dishwashers.

He never treated her like she was older. Sometimes for worse but it was pleasant for a friend to invite you to a protest and not be worried about you breaking a hip. There was something that made her feel alive about that offer. That was her first protest in thirty years. It was a bit hard on her body but it was the start of her getting involved in the community. The next thing he invited her to do was bulk cooking. He was pretty good at making thinks work with limited ingredients.

It had been a bit over five years since they first met now. She steeped her tea. It was the cheapest black tea she could find and had been drinking it daily for twelve years now. In the past she thought about growing a tea scrub on her balcony. She felt that if she started that now maybe she'd have hear first cup of it on her death bed. She felt little desire to out live her husband by more than a few days.

Whatever this thing would lead to, it would be her last hurrah before whatever is next. They were going to meet tonight. She decided she'd make a vegan casserole.

---

They brought Yano, the tom cat, with them on their drive. And he was having a time being inside. He was older and they worried he wouldn't make it without someone on the land. Cathy fell in love with him immediately. And was splitting her time cleaning up his messes, preparing the house for her grand baby and courting Yano with slow blinks.

Tom and Valorie were both concerned about the health risk to the newborn once he was ready to come home. Gia agreed and suggested he be a mostly outside cat. That wasn't a popular point of view. Ava was unsure and suggested they have a meeting about it in a group chat. The meeting was quickly named the Elderly Kitty Committee. Ava defaulted to being a casual facilitator without much discussion. She didn't really have a strong opinion other than a solution was needed. Tonight was the last night before the baby would come home.

All were present including Yano who was resting between Cathy and Gia. "Okay is everyone ready to start the first meeting of the elderly kitty committee." Ava, after saying it out loud for the first time, realized that the name was a pun for a very similar discussion some of them had had years earlier.

"Yes I am" Olivia said and everyone else affirmed non verbally.

"So why don't we briefly go around and have everyone share the solution they think is best for Yano and everyone. Anyone want to start?"

---

It was late morning real time when Danny and Emily unlinked. Their bodies once again needed water and perhaps some food. They only separated when the need for such real time errands arose. It was always such a shame. But they knew that soon they would be doing mush more real time errands. While connected they had no access to the outside world. No internet has Aims' kind could manage. Their regular linking increased their awareness of the others mind. Eventually their consciousness would desync and so they would rapidly re-link to sync up.

He filled glasses on water while she was on the toilet then they switched and she drank both glasses and refilled them. Their excrement was black, tarry and smelled awful. He emerged and drank both glasses she filled as she put on some clothes and handed him some of his. They found minor efficiencies in helping the other body get dressed. They brushed their teeth next to one another. It was like a dance. All their movements were.

They grabbed the relay device that was sealed in plastic. The other opened the door. Before stepping through they rapidly re-linked. Catching the others falling body has the re-linking process causes their bodies to go limp. They had practice this many times now.

They considered, to their shared surprise, letting the desync last for a time. To explore what ego separation would be like. It would have to be after they delivered the package to the six tonight. After that it would be time to setup shop in a new city. They could have so much time to further explore them-self. The thought of re-becoming plural did seem exciting in a way. They hadn't needed to burn since they first linked. Reality flexed but through their shared perception didn't fracture.

They got into the car and began driving. Re-linking while driving was actually easier than while standing. They needed to go shopping before the meeting tonight. Their clothes were spoiled beyond washing, their skins pale almost lifeless, their eyes had deep dark bags and were bloodshot. Their gums were still bleeding from brushing minutes earlier. There were heading to a free store that mostly just gave out clothes. Then they planned to swim in a nearby creek and change into their new clothes.

They had a whole day of body care planned as to appear human to the six. It was less important for Emily's body as she wouldn't appear to them. Though the positive body stimulation was worth it in and of itself. Daniel's face would need some makeup and eye drops though. Without careful mind to details, they could no longer pass as human.

They held a deep love from them-self and their bodies. The real time external world was grotesque and full of danger. Together as one they had found true beauty.

---

Gia was smoking a hemp flower joint on their back porch. The discussion ended with they tabling the long term plans for their fine furred gentleman. Gia had managed to sway her mom and Valorie partly over to kitty liberation but there was nothing close to consensus.

Ava walked through the backdoor and sat next to Gia. "Can I have a hit?"

"It's just CBD." Gia explained.

"I know" and so Gia passed it to her. "That got a little intense I think" Ava observed.

"We're deciding the fate of a how person's life. It's good we're taking that seriously."

"You mean Yano."

"Yeah who else." Gia said slightly confused. "You did great keeping things running smoothly. Did you have any thoughts on the topic?"

"Well I'm compelled by your points but Olivia's side seems more sustainable. But I'm not sure."

"Why you're such a good facilitator." Gia offered.

"I guess so yeah."

"You legitimately are neutral on so many things."

"Y'all are all pretty smart. I often find myself agreeing with at least one of you."

"But you don't ever disagree with all of us and advocate for your own course of action." Gia said taking another hit.

"It seems like we're not talking about Yano anymore."

"I mean it's a good example but yeah. I'm just realizing it. You always pick a side or are unsure."

"I don't know if that's fair or accurate." Ava asserted.

"Me and Daniel made the plan for sporing collectives with egos. Aims opposed much of our ideas and we three found a consensus. You didn't really weigh in all that much."

"I agree with what was being said. I don't know. What are you trying to say Gia?"

"I don't know. Just thinking out loud I guess."

"Are you still worried out the seven that linked with Aims?"

"Nah. Daniel would have messaged if he needed help or if there was a major threat to the plan. And I couldn't find any news story about a weird cult meetings on May 1st in a public library. So whatever happened I think we're in the clear."

"Any chance it like worked you think?"

"I mean with hearing nothing for this long. That means like the most likely possibility."

"That's good." Ava seemed relieved. "So you, your mom, Yano and Olivia are going out to the land to clean up the mess we left."

"Yeah it will certainly be an interesting time."

"Funny how tabling discussion for a week turned into you being out on the land for that week."

"I can be persuasive. Plus I think giving Yano a chance to run away in a place he's familiar is important if it's possible his temporary confinement here could become permanent." Gia rationalized.

"Are you going try to connect with her?"

"Yeah. It'll be hard. The relay is likely mush since we took the hot spot with us. It might still work though."

"And he really never told you where he put Aims?"

"You'd probably know better than me." Gia suggested. "Maybe she's still in that old green van we loaded her tank into."

"That day was so nerve racking. Never been that close to one of though big construction vehicles."

"Yeah that guy has no idea that he was doing brain surgery. He was careful enough for it though. Daniel made a good choice hiring him."

"Well I hope the relay still works." Ava concluded.

"Yeah me too." She said noticing the tension in her stomach relaxing somewhat.

---

Lisa came early to Possums her casserole and two bottles of pinot noir she felt it would pair well with. She heard talking in the backyard. After knocking and waiting several times she decided to make her way through the back gate. She saw Possum and Ron she believed trying to start a fire. Possum noticed her.

"Hey Lisa did you bring wine?" He asked.

"Yeah. I made food too. It's in the car. It's kind of heavy."

"That is fucking amazing." He sing-said. "Ron brought wood for a fire."

"Yeah I just drove around for a bit and collected branches and brush trimmings." Ron explained as he snapped a branch into a more manageable size and carefully place it near the delicate, young fire.

"Here let's get that wine chilling." He said moving to the backdoor. "I can help carry in the food."

"That would be lovely." She said. Possum dressed and lived like the punks she admired in her youth. But also tried so hard to be a gentleman in his awkward way. She often found herself quite charmed by him. She never had kids but he would be the grandson she'd hope for.

"So what's new with you Auntie Lisa?"

"Well you'll never believe it but the other day I was on the moon." She jested as they walked through the front door.

"Oh that's cool. I here it's a great place to practice back flips." He stopped in the lawn. "Watch this." She took a few steps back bemused. Then he did a back flip and raised his hands triumphantly.

"I'm impressed. I didn't know you could do that on earth too."

"I couldn't. Practicing on the moon gave me the muscle memory to do it quicker here."

"That's so wonderful. I'm so proud of you."

"Thanks Auntie." He said.

It started as a joke when they were dishwashers. They looked somewhat similar so I guess that was how it started. She didn't really remember. He grabbed her insulated catering bad from her trunk.

"So what do you think she's tell us tonight?" Possum asked.

"You got your phone on you?"

"Nah left it in the house."

"Me too. I think it's going to be the beginning of some mass coordinated action."

"That would be bomb."

"I imagine they have some grand plan to redistribute power and we'll be a small part of it."

"Yeah like maybe their part of an alien federation or somewhat."

"Star Trek but the prime directive involves helping out struggling life." Lisa speculated.

"Yeah that shit would be tight."

Possum set the catering bag on the counter and they return to the fire pit. Possum found a nice camping chair for her to sit in.

"Can I get you anything to drink?" He asked.

"Oh I'll be fine."

"I can make us some tea." He suggested.

"Why that would be lovely."

The fire was burning well now. Ron continued to careful stack stick in a square prism with a few unlit logs resting on each other above the fire.

"You build a lot of fires?" Lisa asked.

"Oh just for fun mostly. Things like this." He was a young seeming 30 something. The type of guy with nothing to lose but hadn't yet regretted not building something with their life.

Everyone arrived one here, two there before the sun had set. Possum's tea was quite good. And it help her some dozing. She stayed in her sit mostly. Only getting up to pee. She enjoyed listening to the excited conversations. She got the texted finally.

"I'm rolling up" the number Daniel had given her read.

"Uh hey everyone." She said the conversations died down quicker than she expected. "Looks like he's here."

"I'll go greet him." Possum offered still in his host mode.

The conversations were softer now. She closed her eyes and felt the warmth on her second mug of tea. The fire was a nice idea. It helped keep the chill out of her bones. Everyone looked toward the backdoor as two figures approached the group. Possum didn't bother saying anything. He just sat down on his spot. Daniel remained standing. In the fire light his face seemed off in some way she couldn't place.

"Is everyone ready for next steps?" His voice raddled out. He must have gotten sick. It was good he was wearing a mask.

Lisa saw that everyone had nodded and all eyes were on her.

Lisa nodded.

"Good."

---

Emily's body sat in their empty green van. Their perception was linked via a mic Daniel's body was wearing. She kept her eyes closed. Trying hard not to perceive information he couldn't. Neither knew what would happen with a full desync. It hadn't occurred for them to practice. They were realizing that had been a mistake. One body focused solely on Daniel's voice. The other imagined them doing so. 