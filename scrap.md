Gia awoke in bed with Olivia holding her. Gia didn't know how she got to bed. Ava and Valorie were asleep on the mattress on the floor. *Did I even get out of bed?* What a weird and real feeling dream. Gia looked around her mostly the same since high school bedroom.

The things her waking mind focused on were what was different. Cardboard boxes stacked and pushed to a corner where her desk and computer used to be. 

---

The impromptu picnic in a meadow Gia presumed was far enough was winding down. Gia finished the last bite of a sandwich Ava had made for the occasion. Olivia head was in Gia's lap with one hand in one of Ava's and another touching the soil.

"Can I take a photo?" Cathy asked. They all looked at each other.

"Sure." Ava smiled. Olivia looked up at Gia and Gia looked past her mother's phone toward the treeline their house must be. The patterned floral blanket underneath the three. It was a nice photo Cathy thought.

"So do y'all ever feel jealous?" Cathy asked.

"All the time" Olivia started still looking up at Gia.

"It's not a bad thing just a feeling" Ava added.

---


You were easy to make smile then. That was part part of how you'd learn to connection. You had to want to comeback. It didn't matter what you did really. The maintaining friendship was the goal. You were riding on a giant bird that was your best imagining on an albatross. Back then what you saw was only what you imagined.

## From a possible chapter V
Valorie was driving us all back to see if the fire had hit the house. Sleeping in the gymnasium was hard. In fact, Gia considered that she hadn't actually managed it. She sat in the cramped rear facing seat. Her bag now full of dirty clothes her pillow. She must of fallen asleep because in the next moment the car was stopped. Valorie was talking to someone from the driver seat. Pulsing lights hit the interior indicating emergency workers. *Shit*

Gia laid still then slowly deflated to be lower and more out of sight. *It's better to be not seen when there are people with guns that can kidnap you.* Gia listened to the conversation.

"So you're the home owner?" the cop asked.

Valorie looked at Cathy. "Yes I am sir." Cathy said.

"Sorry to hear that. Okay." the cop said. "Okay let them through."

The car began moving. And after a few minutes it was on a gravel drive she knew. Gia looked up. Everyone was staring outside of the car looking at what wasn't there. She was home. Valorie park the car where it was the night before. Potholes in the drive the only familiar point of reference. Gia got out of the car through the hatch back. And began walking.

"Where are you going hun?" Valorie asked.

"I want to walk around." Gia said. "Alone though I need to be alone for a bit." As Gia walked away she heard them all talking. Making plans. Gia did her part for them and they can take it from here. Aims needed here and Gia had to know she was okay.

---

It was midday when Olivia found her lay of the rock Aims said she'd be under. "How are you?" Olivia said as she sat down next to Gia on her right.

Gia felt terrified. "I didn't sleep and all the anxiety was really hitting me earlier. Needed to be away."

"It was really cool how you took charge last night. Your mom's convinced you saved everyone. I'm inclined to agree." Olivia smirked. Olivia has so many cute and kind of just not quite right facial expressions. They're consistent though.

"I guess I just entered doing an action mode." Gia said. *I can tell her everything. She would understand. She would help. She would keep me from finding Aims. I have to find Aims first.* "I knew the where the fire was because I was sitting here." Gia lied. "I saw how close it was and I ran over here."

"What were you doing out here at night?"

"This is the spot. That I'd connect to mushroom girl when I was a kid. And recently I guess." Gia said.

"Where you talking when the fire started?"

Gia considered this fictional timeline. "No. I just walked up here after working with Ava on the tiller. I saw the fire immediately and came back."

"That makes sense. So this is weird right? Like this weird freak fire happens. And right before you're parents were going to sell the land."

"Fortunate for the potential buyer I guess." Gia mused.

"Where did you get that jacket anyways. And the shirt and the pants. I've never seen you in any of this before we here."

"Old high school clothes. Was trying them out. She what I like. Now it's all I got to wear."

"Wasn't your bag in the back last night?"

"I packed it with so stuff we'd need and things my parent would be sad if they lost." Gia said. "Nothing really that precious in my bag."

"That's an okay cover story but we can workshop so of those details." Olivia said with a smile that wasn't hers.

Gia found herself in a meadow with trees and without Olivia. "You have to stop pretending to be Olivia Aims." Gia looked to her left and saw Aims looking mischievous.

"Well the first part of the conversation was Olivia. In fact she's still right there real time in front of us. I decided to try to help you out." Aims said. "I wanted to give you a fair shot but that story would be too complicated to maintain."